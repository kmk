/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <klocale.h>
#include <kstdaccel.h>
#include <kmenubar.h>
#include <kpopupmenu.h>
#include <kprogress.h>
#include <kmessagebox.h>
#include <kapplication.h>
#include <kconfig.h>
#include <qfiledialog.h>
#include <qtabwidget.h>
#include <qprocess.h>
#include <qdir.h>

#include "tag.h"
#include "fileref.h"
#include "tstring.h"
#include "tfile.h"

#include "kmk.h"
#include "kmksettings_dialog.h"
#include "kmk_progress_disp.h"
#include "kmktaged.h"

#define _KMK_UPDATE_PERIOD 250
#define _KMK_NEWCATALOG "NEW.kmk"
#define A__PLAY		1
#define A__ENQUEUE	0
#undef __KMK_DEBUG

const QString player_bin = "xmms";

kmk::kmk()
    : KMainWindow( 0, i18n("KDE Music Kataloger") )
{
  // allocate settings object
  s = new KmkGlobalSettings;
  // be safe here
  Q_CHECK_PTR( s );
  if ( !s ) kdDebug() << "no mem for s!" << endl;
  // if all is fine - then read the settings
  s->readSettings( kapp->config() );
  // save the caption - this one will be used when we need to restore it
  savedCaption = this->caption();
  init_interface();

  // set config object to read GUI group
  kapp->config()->setGroup( "GUI" );
  // reads the splitter settings from the config object
  splitter1->setSizes( kapp->config()->readIntListEntry( "splitter config" ) );
  // then moves the window to its last position
  move( kapp->config()->readPointEntry( "main win position" ) );

  kmkSmooth = new QTime();
  kmkProgress = new kmk_progress_disp();
  kmkProgressTimer = new QTimer();

  player = new KmkExtPlayer( this, 0 );

  KPopupMenu * file = new KPopupMenu( this );
// Actions
  fileNewAction = KStdAction::openNew(this,
                                      SLOT(slotFileNew()),actionCollection());
  fileOpenAction = KStdAction::open(this,
                                    SLOT(slotFileOpen()),actionCollection());
  catalogAddNewFolderAction = new KAction( i18n("Add new folder to catalog"), KShortcut(""), this,
                                    SLOT(slotCatalogAddNewFolder()), actionCollection(), "add_new_folder" );
  fileSaveAction = KStdAction::save(this,
                                    SLOT(slotFileSave()),actionCollection());
  fileSaveAsAction = KStdAction::saveAs(this,
                                        SLOT(slotFileSaveAs()),actionCollection());
  fileCatalogFileStats = new KAction( i18n("Catalog statistics"), KShortcut(""), this,
                                    SLOT(slotCatalogFileStats()), actionCollection(), "catalog_stats" );
  fileCatalogFileClose = KStdAction::close(this,
                                        SLOT(slotCatalogFileClose()),actionCollection());
  fileQuitAction = KStdAction::quit(this,
                                    SLOT(slotFileQuit()),actionCollection());
  listTableToggleTitleAction = new KAction( i18n("Toggle Title"), KShortcut(""), this,
                                    SLOT(slotListTableToggleTitle()), actionCollection(), "toggle_title" );
  TagEditAction = new KAction( i18n("Tag editor"), KShortcut(""), this,
                                    SLOT(slotTagEdit()), actionCollection(), "tag_editor" );
  listTableLocateAction = new KAction( i18n("Locate"), KShortcut(""), this,
                                    SLOT(slotLocateRequested()), actionCollection(), "locate" );
  programSettingsAction = KStdAction::preferences(this,
                                    SLOT(slotProgramSettings()),actionCollection());
  playerPreviousAction = new KAction( i18n("Previous "), KShortcut("ALT+Z"), this,
                                    SLOT(slotPlayerPrevious()), actionCollection(), "prev");
  playerPlayAction = new KAction(i18n("Play "), KShortcut("ALT+X"), this,
                                    SLOT(slotPlayerPlay()), actionCollection(), "play");
  playerPauseAction = new KAction(i18n("Pause "), KShortcut("ALT+C"), this,
                                    SLOT(slotPlayerPause()), actionCollection(), "pause");
  playerStopAction = new KAction(i18n("Stop "), KShortcut("ALT+V"), this,
                                    SLOT(slotPlayerStop()), actionCollection(), "stop");
  playerNextAction = new KAction(i18n("Next "), KShortcut("ALT+B"), this,
                                    SLOT(slotPlayerNext()), actionCollection(), "next");
  playerEnqueueAction = new KAction(i18n("Enqueue selection "), KShortcut(""), this,
                                    SLOT(slotPlayerEnqueue()), actionCollection(), "enqueue");
  playerEnqueueDirAction = new KAction(i18n("Enqueue selected folder ONLY "), KShortcut(""), this,
                                    SLOT(slotPlayerEnqueueDir()), actionCollection(), "enqueue_dir");
  playerEnqueueDirSubdirsAction = new KAction(i18n("Enqueue folder with subfolders "), KShortcut(""), this,
                                    SLOT(slotPlayerEnqueueDirSubdirs()), actionCollection(), "enqueue_dir_subdirs");
  playerPlayDirAction = new KAction(i18n("Play selected folder ONLY "), KShortcut(""), this,
                                    SLOT(slotPlayerPlayDir()), actionCollection(), "play_dir");
  playerPlayDirSubdirsAction = new KAction(i18n("Play folder with subfolders "), KShortcut(""), this,
                                    SLOT(slotPlayerPlayDirSubdirs()), actionCollection(), "play_dir_subdirs");
  playerPlaySelectionAction = new KAction(i18n("Play selection "), KShortcut(""), this,
                                    SLOT(slotPlayerPlaySelection()), actionCollection(), "play_sel");
  catalogUpdateSubtreeAction = new KAction(i18n("Update catalog subtree "), KShortcut(""), this,
                                    SLOT(slotCatalogUpdateSubtree()), actionCollection(), "update_subtree");

// File menu actions
  fileNewAction->plug( file );
  fileOpenAction->plug( file );
//  catalogAddNewFolderAction->setEnabled( FALSE );
  catalogAddNewFolderAction->plug( file );
  fileSaveAction->setEnabled( FALSE );
  fileSaveAction->plug( file );
  fileSaveAsAction->setEnabled( FALSE );
  fileSaveAsAction->plug( file );
  fileCatalogFileStats->setEnabled( FALSE );
  fileCatalogFileStats->plug( file );
  fileCatalogFileClose->setEnabled( FALSE );
  fileCatalogFileClose->plug( file );
  file->insertSeparator();
  fileQuitAction->plug( file );
  menuBar()->insertItem( i18n("Catalog"), file );
// Tools menu actions
  file = new KPopupMenu( this );
  TagEditAction->plug( file );
  menuBar()->insertItem( i18n("Tools"), file );
// Player controls menu actions
  file = new KPopupMenu( this );
  playerPreviousAction->plug( file );
  playerPlayAction->plug( file );
  playerPauseAction->plug( file );
  playerStopAction->plug( file );
  playerNextAction->plug( file );
  menuBar()->insertItem( i18n("Player controls"), file );
// Settings menu actions
  file = new KPopupMenu( this );
  programSettingsAction->plug( file );
//  listTableToggleTitleAction->plug( file );
  menuBar()->insertItem( i18n("Settings"), file );
// Help menu actions
  file = helpMenu();           menuBar()->insertItem( i18n("Help"), file );

// Toolbar menu
  KToolBar * fileToolsToolbar = new KToolBar( this, "File operations" );
  fileToolsToolbar->setOrientation( Qt::Horizontal );
  fileToolsToolbar->setIconText( KToolBar::IconOnly );
  fileToolsToolbar->setLabel( i18n("File operations") );

  fileNewAction->plug( fileToolsToolbar );
  fileOpenAction->plug( fileToolsToolbar );
  fileSaveAction->plug( fileToolsToolbar );
  fileToolsToolbar->insertSeparator();
  fileQuitAction->plug( fileToolsToolbar );

  KToolBar* playerActionsToolbar = new KToolBar( this, "Player controls" );
  playerActionsToolbar->setOrientation( Qt::Horizontal );
  playerActionsToolbar->setIconText( KToolBar::TextOnly );
  playerActionsToolbar->setLabel( i18n("Player controls") );
  playerPreviousAction->plug( playerActionsToolbar );
  playerPlayAction->plug( playerActionsToolbar );
  playerPauseAction->plug( playerActionsToolbar );
  playerStopAction->plug( playerActionsToolbar );
  playerNextAction->plug( playerActionsToolbar );

  moveDockWindow( fileToolsToolbar, Top );
  moveDockWindow( playerActionsToolbar, Top );

// popup menus
  CTree_PopupMenu = new QPopupMenu( this, "catalog tree popup" );
  fileNewAction->plug( CTree_PopupMenu );
  catalogAddNewFolderAction->plug( CTree_PopupMenu );
  CTree_PopupMenu->insertSeparator(2);
  playerPlayDirAction->plug( CTree_PopupMenu );
  playerPlayDirSubdirsAction->plug( CTree_PopupMenu );
  CTree_PopupMenu->insertSeparator(5);
  playerEnqueueDirAction->plug( CTree_PopupMenu );
  playerEnqueueDirSubdirsAction->plug( CTree_PopupMenu );
  CTree_PopupMenu->insertSeparator(8);
  catalogUpdateSubtreeAction->plug( CTree_PopupMenu );

  CList_PopupMenu = new QPopupMenu( this, "files list popup" );
  playerPlaySelectionAction->plug( CList_PopupMenu );
  playerPlaySelectionAction->setEnabled( FALSE );
  playerEnqueueAction->plug( CList_PopupMenu );
  playerEnqueueAction->setEnabled( FALSE );
  CList_PopupMenu->insertSeparator(2);
  TagEditAction->plug( CList_PopupMenu );
  TagEditAction->setEnabled( FALSE );

  CSearchList_PopupMenu = new QPopupMenu( this, "search list popup" );
  playerPlaySelectionAction->plug( CSearchList_PopupMenu );
  playerEnqueueAction->plug( CSearchList_PopupMenu );
  CSearchList_PopupMenu->insertSeparator(2);
  TagEditAction->plug( CSearchList_PopupMenu );
  listTableLocateAction->plug( CSearchList_PopupMenu );

  if( s->dbg() & KMK_DBG_SIGNALS ) kdDebug() << "constructor: connecting signals to slots..." << endl;

  connect( treeListView, SIGNAL( contextMenuRequested( QListViewItem*, const QPoint &, int ) ),
           this,                 SLOT( slotTreeListViewPopupMenuRequested( QListViewItem*, const QPoint &, int ) ) );
  connect( filesListView,      SIGNAL( contextMenuRequested( QListViewItem*, const QPoint &, int ) ),
           this,                      SLOT( slotFileListTablePopupMenuRequested( QListViewItem*, const QPoint &, int ) ) );
  connect( searchFilesListView,    SIGNAL( contextMenuRequested( QListViewItem*, const QPoint &, int ) ),
           this,                          SLOT( slotSearchListTablePopupMenuRequested( QListViewItem*, const QPoint &, int ) ) );
  connect( treeListView, SIGNAL( currentChanged( QListViewItem* ) ),
           this,                 SLOT( slotTreeListViewCurrentChanged( QListViewItem* ) ) );
  connect( leSearchFor,    SIGNAL( returnPressed() ),
           this,                             SLOT( slotSearchButtonClicked() ) );
  connect( pbSearch,  SIGNAL( clicked() ),
           this,                    SLOT( slotSearchButtonClicked() ) );
  connect( pbClear,    SIGNAL( clicked() ),
           this,                         SLOT( slotClearButtonClicked() ) );
  connect( pbDone, SIGNAL( clicked() ),
           this,                     SLOT( slotDoneButtonClicked() ) );

  if( s->dbg() & KMK_DBG_SIGNALS ) kdDebug() << "constructor: done!" << endl;

  FileSaveCalledFromFileSaveAs = FALSE;
  tree_needs_update = FALSE;

  QDir tmp_dir;   tmp_dir.mkdir( "/tmp/kmk", TRUE );

  setAutoSaveSettings();
  applyMainWindowSettings( kapp->config(), QString::fromLatin1("MainWindow") );

  clearCatalogData();
  if ( s->loadLast() )
   {
    if ( !s->lastCatalogUsed().isEmpty() )
     {
       show();
       loadCatalog( s->lastCatalogUsed() );
       update_tree_view();
     }
   }
}

void kmk::init_interface()
{
    main_container_widget = new QWidget( this, "main_container_widget" );

    setCentralWidget( main_container_widget );

    kmkwidgetbaseLayout = new QVBoxLayout( main_container_widget, 11, 6, "kmkwidgetbaseLayout");

    tabWidget1 = new QTabWidget( main_container_widget, "level1_tabwidget" );

    tab = new QWidget( tabWidget1, "level1_tab1" );
    tabLayout = new QVBoxLayout( tab, 11, 6, "tab1_Layout");

    splitter1 = new QSplitter( tab, "splitter1" );
    splitter1->setMinimumSize( QSize( 182, 60 ) );
    splitter1->setOrientation( QSplitter::Horizontal );

    treeListView = new kmk_tree_KListView( splitter1, "treeListView" );
    treeListView->addColumn( i18n( "Catalog tree          " ) );
    treeListView->setResizePolicy( KListView::AutoOneFit );
    treeListView->setShowSortIndicator( TRUE );
    treeListView->setRootIsDecorated( TRUE );
    treeListView->setItemsMovable( TRUE );
    treeListView->setDragEnabled( TRUE );
    treeListView->setDropVisualizer( TRUE );

    filesListView = new kmk_files_KListView( splitter1, "filesListView" );
    filesListView->addColumn( i18n( "Folder" ) );
    filesListView->addColumn( i18n( "Filename" ) );
    filesListView->addColumn( i18n( "Artist" ) );
    filesListView->addColumn( i18n( "Title" ) );
    filesListView->addColumn( i18n( "Album" ) );
    filesListView->addColumn( i18n( "Genre" ) );
    filesListView->addColumn( i18n( "Comment" ) );
    filesListView->addColumn( i18n( "Year" ) );
    filesListView->addColumn( i18n( "Track #" ) );
    filesListView->addColumn( i18n( "Duration" ) );
    filesListView->addColumn( i18n( "Size" ) );
    filesListView->addColumn( i18n( "Bitrate" ) );
    filesListView->addColumn( i18n( "Sampling rate" ) );
    filesListView->addColumn( i18n( "Channels" ) );
    filesListView->addColumn( i18n( "Type" ) );
    filesListView->addColumn( i18n( "READ ONLY" ) );
    filesListView->setResizePolicy( KListView::AutoOneFit );
    filesListView->restoreLayout( kapp->config(), "listView" );
    filesListView->setAllColumnsShowFocus( TRUE );
    filesListView->setShowSortIndicator( TRUE );
    filesListView->setRootIsDecorated( FALSE );
    filesListView->setItemsMovable( FALSE );
    filesListView->setSelectionMode( QListView::Extended );
    filesListView->setDragEnabled( TRUE );
    filesListView->setDropVisualizer( TRUE );
    tabLayout->addWidget( splitter1 );
    tabWidget1->insertTab( tab, i18n("Catalog") );

    tab_2 = new QWidget( tabWidget1, "level1_tab2" );
    tabLayout_2 = new QVBoxLayout( tab_2, 11, 6, "tab2_Layout");

    layout5 = new QVBoxLayout( 0, 0, 6, "layout5");

    tabWidget2 = new QTabWidget( tab_2, "level2_tabwidget" );
    tabWidget2->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)0, 0, 0,
                               tabWidget2->sizePolicy().hasHeightForWidth() ) );
    tabWidget2->setMinimumSize( QSize( 480, 158 ) );

    tab_3 = new QWidget( tabWidget2, "level2_tab1" );

    pbSearch = new QPushButton( tab_3, "pbSearch" );
    pbSearch->setGeometry( QRect( 370, 10, 100, 26 ) );
    pbSearch->setText( i18n( "Go!" ) );
    pbSearch->setDefault( TRUE );

    pbClear = new QPushButton( tab_3, "pbClear" );
    pbClear->setGeometry( QRect( 370, 50, 100, 26 ) );
    pbClear->setText( i18n( "Clear" ) );

    pbDone = new QPushButton( tab_3, "pbDone" );
    pbDone->setGeometry( QRect( 370, 90, 100, 26 ) );
    pbDone->setText( i18n( "Done" ) );

    frame1 = new QFrame( tab_3, "frame1" );
    frame1->setGeometry( QRect( 10, 10, 350, 110 ) );
    frame1->setFrameShape( QFrame::StyledPanel );
    frame1->setFrameShadow( QFrame::Raised );

    textLabel1 = new QLabel( frame1, "textLabel1" );
    textLabel1->setGeometry( QRect( 10, 10, 100, 16 ) );
    textLabel1->setText( i18n( "Look for" ) );

    leSearchFor = new KLineEdit( frame1, "leSearchFor" );
    leSearchFor->setGeometry( QRect( 10, 30, 330, 24 ) );
    tabWidget2->insertTab( tab_3, i18n("Base") );

    tab_4 = new QWidget( tabWidget2, "level2_tab2" );
    tabWidget2->insertTab( tab_4, i18n("Advanced") );
    layout5->addWidget( tabWidget2 );

    searchFilesListView = new kmk_files_KListView( tab_2, "searchFilesListView" );
    searchFilesListView->addColumn( i18n( "Folder" ) );
    searchFilesListView->addColumn( i18n( "Filename" ) );
    searchFilesListView->addColumn( i18n( "Artist" ) );
    searchFilesListView->addColumn( i18n( "Title" ) );
    searchFilesListView->addColumn( i18n( "Album" ) );
    searchFilesListView->addColumn( i18n( "Genre" ) );
    searchFilesListView->addColumn( i18n( "Comment" ) );
    searchFilesListView->addColumn( i18n( "Year" ) );
    searchFilesListView->addColumn( i18n( "Track #" ) );
    searchFilesListView->addColumn( i18n( "Duration" ) );
    searchFilesListView->addColumn( i18n( "Size" ) );
    searchFilesListView->addColumn( i18n( "Bitrate" ) );
    searchFilesListView->addColumn( i18n( "Sampling rate" ) );
    searchFilesListView->addColumn( i18n( "Channels" ) );
    searchFilesListView->addColumn( i18n( "Type" ) );
    searchFilesListView->addColumn( i18n( "READ ONLY" ) );
    searchFilesListView->setResizePolicy( KListView::AutoOneFit );
    searchFilesListView->restoreLayout( kapp->config(), "searchListView" );
    searchFilesListView->setAllColumnsShowFocus( TRUE );
    searchFilesListView->setShowSortIndicator( TRUE );
    searchFilesListView->setRootIsDecorated( FALSE );
    searchFilesListView->setItemsMovable( FALSE );
    searchFilesListView->setDragEnabled( TRUE );
    searchFilesListView->setDropVisualizer( TRUE );
    searchFilesListView->setSelectionMode( QListView::Extended );
    layout5->addWidget( searchFilesListView );
    tabLayout_2->addLayout( layout5 );
    tabWidget1->insertTab( tab_2, i18n("Search") );
    kmkwidgetbaseLayout->addWidget( tabWidget1 );
//    resize( QSize(742, 507).expandedTo(minimumSizeHint()) );
//    clearWState( WState_Polished );

    // tab order
    setTabOrder( tabWidget1, treeListView );
    setTabOrder( treeListView, filesListView );
    setTabOrder( filesListView, tabWidget2 );
    setTabOrder( tabWidget2, leSearchFor );
    setTabOrder( leSearchFor, pbSearch );
    setTabOrder( pbSearch, pbClear );
    setTabOrder( pbClear, pbDone );
    setTabOrder( pbDone, searchFilesListView );
}

kmk::~kmk()
{

  filesListView->saveLayout( kapp->config(), "listView" );
  searchFilesListView->saveLayout( kapp->config(), "searchListView" );

  if( s->dbg() & KMK_DBG_OTHER ) kdDebug() << "destructor: shutting down kmk "VERSION"..." << endl;
  saveMainWindowSettings( kapp->config(), QString::fromLatin1("MainWindow") );
  Q_CHECK_PTR( s );
  switch ( CatalogState )
   {
     case NoCatalog:
        s->setLastCatalogUsed( "" );
        break;
     case Modified:
     case Saved:
        s->setLastCatalogUsed( catalogFileName );
        break;
     default:
        break;
   }
  s->saveSettings( kapp->config() );

  kapp->config()->setGroup( "GUI" );
  kapp->config()->writeEntry( "splitter config", splitter1->sizes() );
  kapp->config()->writeEntry( "main win position", pos() );

  clearCatalogData();
  if (kmkProgress) delete ( kmkProgress );
  if (kmkProgressTimer) delete ( kmkProgressTimer );
  if (kmkSmooth) delete ( kmkSmooth );
  if (player) delete ( player );

  if( s->dbg() & KMK_DBG_OTHER ) kdDebug() << "destructor: done!" << endl;

  delete s;
}

void kmk::slotUpdateProgressDisp()
{
  if ( files_to_read )
     kmkProgress->kPrg->setProgress( total_files );
/*  qDebug( " setting progres to "+QString::number( (int)((total_bytes/(float)bytes_to_read)*100 )) );
  qDebug( "        need to read: "+QString::number( bytes_to_read ) );
  qDebug( "        read: "+QString::number( total_bytes ) );*/
}

void kmk::slotFileNew()
{
  if( CatalogState == Modified )
    switch( KMessageBox::questionYesNoCancel( this,
           i18n("The catalog [%1] contains unsaved changes.\n"
                 "Do you want to save the changes?")
               .arg(catalogFileName),
           i18n("KDE Music Kataloger") ) )   {
      case KMessageBox::Yes: // Save
          slotFileSave();
          break;
      case KMessageBox::No: // Discard - just continue
          break;
      case KMessageBox::Cancel: // Cancel creating new catalog
          return;
          break;
     }
  QFileDialog* fd = new QFileDialog( this, "file dialog", TRUE );
  fd->setCaption( i18n("Select directory to scan") );
  fd->setMode( QFileDialog::Directory );
  fd->setFilter( "*" );
/*  KFileDialog * t = new KFileDialog( ":&lt;scandir&gt;", "*", this, i18n("Select directory to scan"), TRUE );
  t->exec(); */
// = KFileDialog::getExistingDirectory( ":&lt;scandir&gt;", this, i18n("Select directory to scan") );
  if ( fd->exec() == QDialog::Accepted ) {
 // reset counters;
    clearCatalogData();
    subDlevel = 0;
    QString dirName = fd->selectedFile();
 // determine bytes to read;
    kmkSmooth->start();
    kmkProgress->kPrg->setTotalSteps(0);
    kmkProgress->kPrg->setProgress(0);
    kmkProgress->show();
    QTime t;    t.start();
    break_long_disk_operation = FALSE;
    bytes_to_read = 0;    files_to_read = 0;
    bytes_to_read_by_traverse( dirName );
    if ( files_to_read )
     {
       if( s->dbg() & KMK_DBG_FS_OPS )
          kdDebug() << "slotFileNew: Time elapsed: " << t.elapsed()
                    << " ms, need to read: " << (bytes_to_read/(1024*1024))
                    << " MB, files to read: " << files_to_read << endl;
       t.restart();
       kmkProgress->kPrg->setTotalSteps( files_to_read );
 // arm timer event; 300 ms interval - can be done in constructor
       connect( kmkProgressTimer, SIGNAL( timeout() ), this, SLOT(  slotUpdateProgressDisp() ) );
       kmkProgressTimer->start(_KMK_UPDATE_PERIOD,FALSE);
 // show widget, displaying load progress
 // start timer
       traverse_tree( dirName );  // should update total_bytes as it scans
       update_tree_view();
       setCatalogStateAndUpdate( Modified );
       average_file_size = ( total_bytes/total_files );
       if( s->dbg() & ( KMK_DBG_FS_OPS | KMK_DBG_OTHER ) )
        {
          kdDebug() << "slotFileNew: Total time: " << t.elapsed() << " ms, read: "
                    << (total_bytes/(1024*1024)) << " MB, files read: " << total_files << endl;
          kdDebug() << "slotFileNew:STATS: total_files = " << total_files << ";" << endl;
          kdDebug() << "slotFileNew:STATS: total_folders = " << total_folders << ";" << endl;
          kdDebug() << "slotFileNew:STATS: total_bytes = " << total_bytes << " B;" << endl;
          kdDebug() << "slotFileNew:STATS: total_playtime = " << total_play_time << " sec;" << endl;
          kdDebug() << "slotFileNew:STATS: average_file_size = " << average_file_size << " B;" << endl;
        }
       kmkProgress->hide();
 // hide widget
 // stop timer
 // Enable actions wich work only with data...
       if( treeListView->firstChild() )
        slotTreeListViewCurrentChanged( (QListViewItem*) treeListView->firstChild() );
       else qWarning( "slotFileNew: NEED SOMEONE TO CREATE THE TREE FOR US...." );
       QTimer::singleShot( _KMK_UPDATE_PERIOD, this, SLOT( slotFileSaveAs() ) );
     }
    else
     {
       setCatalogStateAndUpdate( NoCatalog );
       clearCatalogData();
       subDlevel = 0;
       KMessageBox::sorry( this, i18n("There are no files of supported types in [%1].").arg(dirName),
                                 i18n("KDE Music Kataloger") );
     }
  }
 delete( fd );
}

void kmk::slotFileOpen()
{
  if( CatalogState == Modified )
    switch( KMessageBox::questionYesNoCancel( this,
           i18n("The catalog [%1] contains unsaved changes.\n"
                 "Do you want to save the changes?")
               .arg(catalogFileName),
           i18n("KDE Music Kataloger") ) )   {
      case KMessageBox::Yes: // Save
          slotFileSave();
          break;
      case KMessageBox::No: // Discard - just continue
          break;
      case KMessageBox::Cancel: // Cancel creating new catalog
          return;
          break;
     }
  QFileDialog* fd = new QFileDialog( this, "file dialog", TRUE );
  fd->setCaption( i18n("Select catalog file to load") );
  fd->setMode( QFileDialog::ExistingFile );
  fd->setFilter( i18n("All KMK catalog files %1").arg("(*.kmk)") );
  if ( fd->exec() != QDialog::Accepted )      delete fd;
  else
   {
    QString fileName = fd->selectedFile();    delete fd;
    qApp->processEvents();
    loadCatalog( fileName );
    update_tree_view();
    Q_CHECK_PTR( s );
    s->setLastCatalogUsed( fileName );
    s->saveSettings( kapp->config() );
   }
}

void kmk::slotCatalogAddNewFolder()
{
  QFileDialog* fd = new QFileDialog( this, "file dialog", TRUE );
  fd->setCaption( i18n("Select directory to scan") );
  fd->setMode( QFileDialog::Directory );
  fd->setFilter( "*" );
/*  KFileDialog * t = new KFileDialog( ":&lt;scandir&gt;", "*", this, i18n("Select directory to scan"), TRUE );
  t->exec(); */
// = KFileDialog::getExistingDirectory( ":&lt;scandir&gt;", this, i18n("Select directory to scan") );
  if ( fd->exec() == QDialog::Accepted ) 
   {
    subDlevel = 0;
    QString dirName = fd->selectedFile();
    if ( catalog_has_dir( dirName ) )
     {
      KMessageBox::sorry( this, i18n("Sorry, but the folder [%1]\nis already present in this catalog!")
                          .arg(dirName),i18n("KDE Music Kataloger") );
      delete fd;
      return;
     }
//    clearCatalogData();
 // reset counters;
    Q_ULLONG bc01 = total_bytes;      Q_ULLONG bc02 = total_files;
    Q_ULLONG bc03 = total_folders;    Q_ULLONG bc04 = total_play_time;
    bytes_to_read = 0;    files_to_read = 0;
    total_bytes = 0;      total_files = 0;
    total_folders = 0;    total_play_time = 0;
 // determine bytes to read;
    kmkSmooth->start();
    kmkProgress->kPrg->setProgress(0);
    kmkProgress->show();
    QTime t;    t.start();
    break_long_disk_operation = FALSE;
    bytes_to_read_by_traverse( dirName );
    if( s->dbg() & KMK_DBG_FS_OPS )
     kdDebug() << "slotCatalogAddNewFolder: Time elapsed: " << t.elapsed()
               << " ms, need to read: " << (bytes_to_read/(1024*1024))
               << " MB, files to read: " << files_to_read << endl;
    t.restart();
 // arm timer event
    if ( files_to_read )
     {
      connect( kmkProgressTimer, SIGNAL( timeout() ), this, SLOT(  slotUpdateProgressDisp() ) );
      kmkProgressTimer->start(_KMK_UPDATE_PERIOD,FALSE);
 // show widget, displaying load progress
 // start timer
      traverse_tree( dirName );  // should update total_bytes as it scans
      update_tree_view();
      setCatalogStateAndUpdate( Modified );
//      kdDebug() << "Total time: " << t.elapsed() << " ms, read: " << (total_bytes/(1024*1024))
//                << " MB, files read: " << total_files << endl;
      total_bytes   += bc01;    total_files     += bc02;
      total_folders += bc03;    total_play_time += bc04;
      if ( total_files ) average_file_size = ( total_bytes/total_files );
      if( s->dbg() & KMK_DBG_FS_OPS & KMK_DBG_OTHER )
       {
        kdDebug() << "slotCatalogAddNewFolder:STATS: total_files = " << total_files << ";" << endl;
        kdDebug() << "slotCatalogAddNewFolder:STATS: total_folders = " << total_folders << ";" << endl;
        kdDebug() << "slotCatalogAddNewFolder:STATS: total_bytes = " << total_bytes << " B;" << endl;
        kdDebug() << "slotCatalogAddNewFolder:STATS: total_playtime = " << total_play_time << " sec;" << endl;
        kdDebug() << "slotCatalogAddNewFolder:STATS: average_file_size = " << average_file_size << " B;" << endl;
       }
 // hide widget
      kmkProgress->hide();
 // stop timer
//      QTimer::singleShot( _KMK_UPDATE_PERIOD, this, SLOT( slotFileSaveAs() ) );
     }
    else KMessageBox::sorry( this, i18n("There are no files of supported types in [%1].").arg(dirName),
                                   i18n("KDE Music Kataloger") );
   }
  delete( fd );
}

/** Saves the catalog in memory to the file @p catalogFileName;
 *  If the string, being a filename in @p catalogFileName does not have
 *  a .kmk extension - this slot adds it
 */
void kmk::slotFileSave()
{
  if( CatalogState != Modified ) return;
/* We need to check whether catalogFileName exists, and what permissions it has.
  If there is a problem - inform the user.       */
  bool fileOK=TRUE;
  if( catalogFileName.isEmpty() || catalogFileName.isNull() )
     catalogFileName = QDir::homeDirPath()+"/"+_KMK_NEWCATALOG;
  QFileInfo * _cf = new QFileInfo( catalogFileName );
/* Make sure .kmk is appended to the file name...*/
  if (_cf->extension(FALSE).lower().compare("kmk") != 0)
    { catalogFileName.append(".kmk"); _cf->setFile( catalogFileName ); }
  if( (_cf->exists())&&(!_cf->isWritable()) ){   /* File is NOT OK, existing, but not writable. Inform.  */
    fileOK = FALSE;
    KMessageBox::sorry( this, i18n("Could not open %1 for writing!").arg(catalogFileName),
                                  i18n("KDE Music Kataloger") );
   }
  if( (_cf->exists())&&(!_cf->isFile()) ){      /* File is NOT OK, it is not even a file. Inform. */
    fileOK = FALSE;
    KMessageBox::sorry( this, i18n("The filesystem item %1 exists, but it is not a file!").arg(catalogFileName),
                                  i18n("KDE Music Kataloger") );
   }
  if( (FileSaveCalledFromFileSaveAs) && (_cf->exists())
       && (_cf->isWritable()) )     /* File is OK, but we need to overwrite it... ask what to do. */
   {
    if( KMessageBox::questionYesNo( this,
        i18n("A file called [%1] already exists.\nDo you want to overwrite it?").arg(catalogFileName),
        i18n("KDE Music Kataloger") ) != KMessageBox::Yes )                            fileOK = FALSE;
   }
  FileSaveCalledFromFileSaveAs = FALSE;
  if( fileOK ) /* After the checks above are passed - lets write! */
   {
    QDomDocument doc( "KMK_catalog_file" );   QDomElement  e = doc.createElement( "KMK_catalog_file_data" );
    e.setAttribute( "Version", 1 );                                  doc.appendChild( e );
    QDomElement e2 = doc.createElement( "CatalogFile_Statistics" );    e.appendChild( e2 );
    e2.setAttribute( "Total_files_count", (double) total_files );
    e2.setAttribute( "Total_bytes_count", (double) total_bytes );
    e2.setAttribute( "Total_dirs_count",  (double) total_folders );
    e2.setAttribute( "Total_secs_count",  (double) total_play_time );
    e2.setAttribute( "Average_file_size", (double) average_file_size );
    e2 = doc.createElement( "CatalogFile_TreeDescription" );           e.appendChild( e2 );

    generateListViewXML( treeListView, doc, e2 );

    QDomElement tag;
    e2 = doc.createElement( "CatalogFile_Objects" );                   e.appendChild( e2 );
    e2.setAttribute( "Total_MObjs_count", (double) MusicCatalog.count() );

    for( MmDataList::iterator it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
     {
/* ask our DOM Document object to create a new DomElement, and fill this
   new element with appropriate data */
       tag = doc.createElement( "MObj" );
       tag.setAttribute( "Folder", (*it).Folder().utf8() );
       tag.setAttribute( "Filename", (*it).FileName().utf8() );
       tag.setAttribute( "Artist", (*it).Artist().utf8() );
       tag.setAttribute( "Title", (*it).Title().utf8() );
       tag.setAttribute( "Album", (*it).Album().utf8() );
       tag.setAttribute( "Genre", (*it).Genre().utf8() );
       tag.setAttribute( "Comment", (*it).Comment().utf8() );
       tag.setAttribute( "Year", (*it).Year() );
       tag.setAttribute( "TrackNumber", (*it).TrackNum() );
       tag.setAttribute( "Length", (*it).Length() );
       tag.setAttribute( "Modified", (*it).ModifiedTime() );
       tag.setAttribute( "Size", (*it).FileSize() );
       tag.setAttribute( "Channels", (*it).Channels() );
       tag.setAttribute( "BitRate", (*it).BitRate() );
       tag.setAttribute( "SampleRate", (*it).SampleRate() );
       tag.setAttribute( "IsReadOnly", (  ((*it).IsReadOnly()) ? "YES" : "NO"  ) );
       tag.setAttribute( "IsFolder", (  ((*it).IsDir()) ? "YES" : "NO"  ) );
/* Then, ask the DOM Document to add this new element in the correct place... */
       e2.appendChild( tag );
     }

    QString xml = doc.toString(2);
    QFile file( catalogFileName );
    if ( file.open( IO_WriteOnly ) ) {
        file.writeBlock( xml, xml.length() );
        file.flush();
        file.close();
        this->setCaption( i18n("Catalog [%1] - ").arg(catalogFileName) + savedCaption );
        setCatalogStateAndUpdate( Saved );
    } else QMessageBox::warning( this, i18n("KDE Music Kataloger"), i18n("Could not open %1 for writing!").arg(catalogFileName),
                        QMessageBox::Cancel,QMessageBox::NoButton,QMessageBox::NoButton );
   }
  else  this->setCaption( i18n("Catalog [%1][UNSAVED] - ").arg(catalogFileName) + savedCaption );
}

void kmk::slotFileSaveAs()
{
  QFileDialog* fd = new QFileDialog( 0, "file dialog", TRUE );
  fd->setCaption( i18n("Select file to save catalog to") );
  fd->setMode( QFileDialog::AnyFile );
  fd->setFilter( i18n("All KMK catalog files %1").arg("(*.kmk)") );
  QString fileName;
  if ( fd->exec() == QDialog::Accepted )
   {
    catalogFileName = fd->selectedFile();
    setCatalogStateAndUpdate( Modified );
    FileSaveCalledFromFileSaveAs = TRUE;
    QTimer::singleShot( _KMK_UPDATE_PERIOD, this, SLOT( slotFileSave() ) );
   }
  delete fd;
}

void kmk::slotCatalogFileClose()
{
  if( CatalogState == Modified )
    switch( KMessageBox::questionYesNoCancel( this,
           i18n("The catalog [%1] contains unsaved changes.\n"
                 "Do you want to save the changes?")
               .arg(catalogFileName),
           i18n("KDE Music Kataloger") ) )   {
      case KMessageBox::Yes: // Save
          slotFileSave();
          break;
      case KMessageBox::No: // Discard - just continue
          break;
      case KMessageBox::Cancel: // Cancel opreation
          return;
          break;
     }
// Ensure there is no data laying around...
   clearCatalogData();
}

void kmk::slotCatalogFileStats()
{
  QString tmp = formatted_string_from_seconds( total_play_time );
  KMessageBox::information( 0,
                      i18n("<h3>Catalog statistics</h3>"
                           "<h4>This catalog represents:</h4>"
                           "<pre>Total capacity   :  %1 MB<br/>"
                                "Total files      :  %2<br/>"
                                "Total folders    :  %3<br/>"
                                "Total playtime   :  %4<br/>"
                                "Average filesize :  %5 MB</pre>")
                      .arg(total_bytes/(float)(1024*1024),-8,'f',1).arg(total_files,-8).arg(total_folders,-8)
                      .arg(tmp).arg(average_file_size/(float)(1024*1024),-8,'f',3),
                      i18n("KDE Music Kataloger"));
}

void kmk::slotFileQuit()
{
  close();
}

void kmk::closeEvent( QCloseEvent* ce )
{
  if( !ce )
   {
    if( s->dbg() & KMK_DBG_SIGNALS & KMK_DBG_OTHER )
       kdDebug() << "closeEvent: bad object" << endl;
    return;
   }
  if( CatalogState == Modified )
    switch( KMessageBox::questionYesNoCancel( this,
           i18n("The catalog [%1] contains unsaved changes.\n"
                 "Do you want to save the changes before exiting?")
               .arg(catalogFileName),
           i18n("KDE Music Kataloger") ) )   {
      case KMessageBox::Yes: // Save & Exit
          slotFileSave();
          ce->accept();
          break;
      case KMessageBox::No: // Discard - just Exit
          ce->accept();
          break;
      case KMessageBox::Cancel: // Cancel - no nothing
          ce->ignore();
          break;
     }
  else ce->accept();
}

void kmk::slotProgramSettings()
{
  // WARNING: kmkSettingsDialog has Qt::WDestructiveClose flag set, it WILL destroy itself
  // also, it internally calls exec() - so we only create it, passing relevant data
  // to its contructor
  uint * k = new uint;
  s->saveSettings( kapp->config() );
  new kmkSettingsDialog( k );
//  if( k ) // config changed? if so - re-read it..
  s->readSettings( kapp->config() );
  player->handleConfigChange();
/* If we need to choose the font kmk uses to display the lists and stuff,
   here is how its done:

  QFont f ( "Courier", 16, 50 );
  main_container_widget->setFont( f );
*/
}

void kmk::slotTagEdit()
{
  MmDataPList *files = new MmDataPList; if( !files ) return;
  MmData *tmp = 0;  // zero-init our MmData pointer
  KListViewItem *node = 0;
  if( _popup_at_search )
          node = (KListViewItem*) searchFilesListView->firstChild();
  else
          node = (KListViewItem*) filesListView->firstChild();

  while ( node )
  {
    if ( node->isSelected() )
     {
       tmp = findMmData( node->text( 0 ), node->text( 1 ) );
       if ( tmp ) if ( (*tmp).IsDir()==FALSE ) files->append( tmp );
     }
    node = (KListViewItem*) node->nextSibling();
  }

  if ( files->isEmpty() ) { delete files; return; }

 /** kmkTagEdit's constructor takes care of everything for us -
  *  we don't even need to call show() or exec() - it is done automagically;
  */
  uint k = 0;
  new kmkTagEdit( files, &k );
  if( s->dbg() & KMK_DBG_SIGNALS & KMK_DBG_OTHER )
       kdDebug() << "slotTagEdit: TagEditDialog returns: " << k << endl;
  if( k ) {
     setCatalogStateAndUpdate( Modified );
     slotTreeListViewCurrentChanged( treeListView->currentItem() );
   }
  delete files;
}

void kmk::slotListTableToggleTitle()
{
//  QTable* tbl = ((kmkWidget*) kmk_widg )->FileList();
//  tbl->hideColumn(1);
}

void kmk::slotPlayerPrevious()  { player->previous(); }
void kmk::slotPlayerPlay()      { player->play();     }
void kmk::slotPlayerPause()     { player->pause();    }
void kmk::slotPlayerStop()      { player->stop();     }
void kmk::slotPlayerNext()      { player->next();     }

/**
 *  In this member we use the fact, that when QTable's selectionMode() is MultiRow,
 *  meaning that whole rows selection is only allowed, QTable passes these
 *  rows as seperate selections - 2 successive rows will give us 2 selections
 *  with each selections topRow() and bottomRow() showing the number of the selected
 *  row in the table         ---THIS ALL IS FALSE
 */
void kmk::slotPlayerPlaySelection()
{
  MmDataPList *files = new MmDataPList;
  MmData *tmp = 0;  // zero-init our MmData pointer
  KListViewItem *node = 0;
  if( _popup_at_search)
          node = (KListViewItem*) searchFilesListView->firstChild();
  else
          node = (KListViewItem*) filesListView->firstChild();

  while ( node )
  {
    if ( node->isSelected() )
     {
       tmp = findMmData( node->text( 0 ), node->text( 1 ) );
       if ( tmp ) if ( (*tmp).IsDir()==FALSE ) files->append( tmp );
     }
    node = (KListViewItem*) node->nextSibling();
  }

  QString flNm = "/tmp/kmk/kmk_lst_"+QString::number( random() )+".m3u";
  QFile file( flNm );
  if ( file.open( IO_WriteOnly ) )
   {
    unsigned long files_cnt = 0;
    QTextStream stream( &file );
    stream << "#EXTM3U" << endl;
    for ( tmp = files->first(); tmp; tmp = files->next() )
     {
       stream <<  "#EXTINF:" << tmp->Length() << ", " << tmp->Artist() << " - " << tmp->Title() << endl;
       stream <<  tmp->Folder() << "/" << tmp->FileName()  << endl;
       files_cnt++;
     }
    file.close();

// if we added ANY files, then AND ONLY THEN use the external player
    if( files_cnt ) player->playPlaylist( flNm );

//        file.remove();
   }
  else QMessageBox::warning( this, i18n("KDE Music Kataloger"),i18n("Could not open %1 for writing!").arg(flNm),
       QMessageBox::Cancel,QMessageBox::NoButton,QMessageBox::NoButton );
  delete files;
}

void kmk::slotPlayerEnqueue()
{
  MmDataPList *files = new MmDataPList;
  MmData *tmp = 0;  // zero-init our MmData pointer
  KListViewItem *node = 0;
  if( _popup_at_search)
          node = (KListViewItem*) searchFilesListView->firstChild();
  else
          node = (KListViewItem*) filesListView->firstChild();

  while ( node )
  {
    if ( node->isSelected() )
     {
       tmp = findMmData( node->text( 0 ), node->text( 1 ) );
       if ( tmp ) if ( (*tmp).IsDir()==FALSE ) files->append( tmp );
     }
    node = (KListViewItem*) node->nextSibling();
  }

  QString flNm = "/tmp/kmk/kmk_lst_"+QString::number( random() )+".m3u";
  QFile file( flNm );
  if ( file.open( IO_WriteOnly ) )
   {
    unsigned long files_cnt = 0;
    QTextStream stream( &file );
    stream << "#EXTM3U" << endl;
    for ( tmp = files->first(); tmp; tmp = files->next() )
     {
       stream <<  "#EXTINF:" << tmp->Length() << ", " << tmp->Artist() << " - " << tmp->Title() << endl;
       stream <<  tmp->Folder() << "/" << tmp->FileName()  << endl;
       files_cnt++;
     }
    file.close();

// if we added ANY files, then AND ONLY THEN use the external player
    if( files_cnt ) player->addPlaylist( flNm );

   }
  else QMessageBox::warning( this, i18n("KDE Music Kataloger"),i18n("Could not open %1 for writing!").arg(flNm),
       QMessageBox::Cancel,QMessageBox::NoButton,QMessageBox::NoButton );
  delete files;
}


void kmk::slotPlayerEnqueueDir()
{
  _kmk_include_subdirs = FALSE;  //tell playerDir to NOT include subfolder(s) contents
  playerDir( A__ENQUEUE ); // here we enqueue
}

void kmk::slotPlayerEnqueueDirSubdirs()
{
  _kmk_include_subdirs = TRUE;  //tell playerDir to include subfolder(s) contents
  playerDir( A__ENQUEUE ); // here we enqueue
}

void kmk::slotPlayerPlayDir()
{
  _kmk_include_subdirs = FALSE; //tell playerDir to NOT include subfolder(s) contents
  playerDir( A__PLAY ); // and here we play
}

void kmk::slotPlayerPlayDirSubdirs()
{
  _kmk_include_subdirs = TRUE; //tell playerDir to include subfolder(s) contents
  playerDir( A__PLAY ); // and here we play
}

#define SCUS	"slotCatalogUpdateSubtree: "
void kmk::slotCatalogUpdateSubtree()
{
// init *lv* to currently selected item in the Tree view
  QListViewItem* lv = treeListView->currentItem();
// this one will hold the full-blown path to the folder we should update
  QString folder_name;
// set this folder's parent to "AUTO"
  QString prev_parent = "AUTO";
// this loop extracts a full path of the selected dir in the Tree view by
// going from it to the top, collecting data on the way in *folder_name*
  while ( TRUE ) {
     folder_name.prepend( lv->text(0) );
     if( lv->parent() )
         { if( lv->parent()->text(0).compare("/") != 0 ) folder_name.prepend( "/" ); lv = lv->parent(); }
// when the top is reached - the loop "break"s
     else break;
  }
  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
    kdDebug() << SCUS"should update " << folder_name << endl;
  QDir d ( folder_name );
// try to find folder_name's item in Catalog to get its parent
// TODO the parent's total files needs to be updated!!!
  MmData * f = findMmData( folder_name, d.dirName() );
// if we've found the correct MmData item - save the parent folder (Comment())
// in prev_parent
  if( f ) prev_parent = f->Comment();
// then clean folder_name and all its children from catalog
  if( rem_MmData_Folder( &MusicCatalog, folder_name ) == 0 )
// if the clean was succesfull
   {
    if( subDlevel ) qWarning( "Some of the previous scans did not end clean!" );
    subDlevel = 0;
// and the folder we were passed exists
    if( d.exists() )
/* scan it as new - passing the previous parent_state;
   This preserves the catalog structure; otherwise all updates would
   result as new sub-catalogs, because first level traverse_tree
   sets scanned folder parent to "TREE_BASE", which means new subtree */
    if( traverse_tree( folder_name, prev_parent ) )
        kdDebug() << SCUS"Sanned [" << folder_name << "] and added items." << endl;
    else kdDebug() << SCUS"Scanned [" << folder_name << "], but NO ITEMS were added!" << endl;
   }
// if any of the things done to the catalog need clean up - do so
  if( tree_needs_update )
   {
    tree_needs_update = FALSE;
    update_catalog_stats( &MusicCatalog );
    update_tree_view();
    if( MusicCatalog.isEmpty() ) setCatalogStateAndUpdate( NoCatalog );
    else setCatalogStateAndUpdate( Modified );
   }
}

void kmk::slotTreeListViewPopupMenuRequested( QListViewItem* itm, const QPoint &pos, int col )
{
  /* Take care of annoying warnings ..... */
  Q_UNUSED(itm); Q_UNUSED(col);
  if( treeListView->currentItem()>0 )
    catalogUpdateSubtreeAction->setEnabled( TRUE );
  else
    catalogUpdateSubtreeAction->setEnabled( FALSE );
  CTree_PopupMenu->popup( pos );
}

void kmk::slotTreeListViewCurrentChanged( QListViewItem * itm )
{
  if ( MusicCatalog.isEmpty() ) { kdDebug() << "LVupdate: called with empty catalog" << endl; return; }
//  TagEditAction->setEnabled( FALSE );
  QListViewItem* lv = itm;  QString p;
//  kdDebug() << "treeListView dropHighlighter() says: " << treeListView->dropHighlighter() << endl;

//  QTime ptime; ptime.start();

  while ( TRUE ) {
     p.prepend( lv->text(0) );
     if( lv->parent() ) { if( lv->parent()->text(0).compare("/") != 0 ) p.prepend( "/" ); lv = lv->parent(); }
     else break;
  }
//  kdDebug() << "LVupdate: user clicked: " << p << endl;
//  kdDebug() << "(kmk):LVupdate: determine hole selected folder name time: " << ptime.elapsed() << " ms." << endl;
//  ptime.restart();

// Find the MmData folder item and get the number of files it contains
  MmDataList::iterator it;
  bool found = FALSE;
  for ( it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
   if( ((*it).IsDir()) && ( (*it).Folder().compare(p)==0 ) ){ found = TRUE; break; }
  int folder_files = (found) ? (*it).FileSize() : 0;

//  kdDebug() << "(kmk):LVupdate: locate folder as *MmData time: " << ptime.elapsed() << " ms.  ff=" << folder_files << endl;
//  ptime.restart();

// disble list for manipulation : flicker care
  filesListView->setEnabled( FALSE );
  while( folder_files > filesListView->childCount() )
      new KListViewItem( (QListView*) filesListView, 0 );
  while( folder_files < filesListView->childCount() )  {
      KListViewItem* t = (KListViewItem*) filesListView->lastItem();
      if ( t ) delete t;
  }
//  kdDebug() << "(kmk):LVupdate: table setup time: " << ptime.elapsed() << " ms." << endl;
//  ptime.restart();

// If there are items, go to the next...
  if ( found ) {
   it++;
// ...and find the first MmData item, which is inside folder *p*
// The result should be files, because MmData folder's Folder() holds their full path,
// and there should be only one folder with *p* as Folder(), and we are already past that...
   while ( (( (*it).Folder().compare(p)!=0 )) ) it++;
  }
  QDateTime dt;
  KListViewItem * sel_item = 0;
  KListViewItem * new_item = (KListViewItem*) filesListView->firstChild();
// This loop's main requirement is that files within a dir are consecutive
  for ( int k=0; k<folder_files; it++,k++ )
  {
    if ( !new_item ) qFatal("Something horrible happened!");
    if ( locateMode )
           if ( ( (*it).Folder().compare( DelSelDir ) == 0 ) && ( (*it).FileName().compare( DelSelFile ) == 0 ) )
                     {   sel_item = new_item;   locateMode = FALSE;     }
    new_item->setText( 0, (*it).Folder() );         // folder - for use in play or enqueue file(s) action
    new_item->setText( 1, (*it).FileName() );       // file
    new_item->setText( 2, (*it).Artist() );         // artist
    new_item->setText( 3, (*it).Title() );          // title
    new_item->setText( 4, (*it).Album() );          // album
    new_item->setText( 5, (*it).Genre() );          // genre
    new_item->setText( 6, (*it).Comment() );        // comment
    new_item->setText( 7, ((*it).Year()!=0)?QString::number( (*it).Year() ):"" );      // song year
    new_item->setText( 8, ((*it).TrackNum()!=0)?QString::number( (*it).TrackNum() ):"" );  // track number
    new_item->setText( 9, formatted_string_from_seconds( (*it).Length() ) ); // length
    new_item->setText( 10, QString::number( (*it).FileSize() / 1024 ) + " kB" ); // file size
 //   dt.setTime_t( (*it).ModifiedTime() );
    new_item->setText( 11, QString::number( (*it).BitRate()) );       // bitrate
    new_item->setText( 12, QString::number( (*it).SampleRate()) );    // samplerate
    new_item->setText( 13, QString::number( (*it).Channels()) );      // channels
    QFileInfo ext_info( (*it).FileName() );
    new_item->setText( 14, ext_info.extension(FALSE).upper() );       // type
    new_item->setText( 15, (*it).IsReadOnly()?"YES":"_no_" );         // read only
    new_item = (KListViewItem*) new_item->nextSibling();
  }

//  kdDebug() << "(kmk):LVupdate: table fill time: " << ptime.elapsed() << " ms." << endl;
//  ptime.restart();

// update table contets after manipulation
  filesListView->clearSelection();
// here we update the first 5 columns' width
  Q_CHECK_PTR( s );
// honor user setting whether he/she wants auto-column-width, or likes speed more
  if ( s->autoColumnWidth() )
      for ( ushort k=0; k<7; k++) filesListView->adjustColumn( k );
  if ( sel_item ) {
     filesListView->ensureItemVisible( sel_item );
     filesListView->setSelected( sel_item, TRUE );
     DelSelDir = "";  DelSelFile = "";
  }
  filesListView->setEnabled( TRUE );
}

void kmk::slotFileListTablePopupMenuRequested( QListViewItem* itm, const QPoint &pos, int col )
{
  Q_UNUSED(itm); Q_UNUSED(col);
  _popup_at_search = FALSE;
  CList_PopupMenu->exec( pos );
}

void kmk::slotSearchListTablePopupMenuRequested( QListViewItem* itm, const QPoint &pos, int col )
{
  Q_UNUSED(itm); Q_UNUSED(col);
  _popup_at_search = TRUE;
  CSearchList_PopupMenu->exec( pos );
}

/**
 * This function locates the correct place in the catalog tree, selects the
 * correct folder and then informs the method updating
 * the file list to select AND show the reqested file
 */
void kmk::slotLocateRequested()
{
// this below is for locating results in the same folder to work,
// without the need to manually change the currently selected folder
// !!! kmk_widg->TreeList()->setCurrentItem( listViewRootItem );
  KListViewItem *node = (KListViewItem*) searchFilesListView->firstChild();
  if ( !node ) return;

// inform the apropriate method that we are in locate mode:
// set all relevant vars
  while ( node )
   {
// as we are called - and this is LOCATE slot - only ONE item in the search
// results should be selected; if there are more - they are ignored
     if ( node->isSelected() )
      {
        DelSelDir = node->text( 0 );
        DelSelFile = node->text( 1 );
        locateMode = TRUE;
        break;
      }
     node = (KListViewItem*) node->nextSibling();
   }

//  kdDebug() << " locating " << DelSelDir << " | " << DelSelFile << endl;
  QString ci;
// get a list of all listview items to iterate over them
  QListViewItemIterator it( treeListView );
  while ( it.current() )
   {
    QListViewItem *item = it.current();
    ci = QString::null;
// this while loop exits when it reaches top level; result is slash ("/") separated path in ci
    bool not_found = TRUE;
    while ( not_found ) {
      ci.prepend( item->text(0) );
      if( item->parent() ) { if( item->parent()->text(0).compare("/") != 0 ) ci.prepend( "/" ); item = item->parent(); }
      else break;
    }
//    kdDebug() << " ci now is " << ci << endl;
    item = it.current();
    if ( DelSelDir.compare( ci ) == 0 )
     {
       treeListView->ensureItemVisible ( item );
       if( treeListView->currentItem() == item )
        slotTreeListViewCurrentChanged( item );
       else
        treeListView->setCurrentItem( item );
       not_found = FALSE;
       break;
     }
    ++it;
   }
// switch from search tab to catalog view tab
  tabWidget1->setCurrentPage(0);
}

void kmk::slotSearchButtonClicked()
{
//  if( MusicCatalog.isEmpty() ) return;
  QString p = leSearchFor->text();
//  TagEditAction->setEnabled( FALSE );

//  QTime ptime; ptime.start();

  QDateTime dt;          long new_rows=0;
// disble table for manipulation : flicker care
  searchFilesListView->setEnabled( FALSE );
// Count the actuall matches
  for( MmDataList::iterator it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
   {
     if( (!(*it).IsDir()) && ( ((*it).Folder().compare(p)==0) ||  (((*it).FileName().contains(p,FALSE)>0) ||
           ((*it).Artist().contains(p,FALSE)>0) ) || (((*it).Title().contains(p,FALSE)>0)  ||
           ((*it).Album().contains(p,FALSE)>0) ) ) )
      {       new_rows++;      }
   }
//  kdDebug() <<  new_rows << " rows should be added." << endl;
// If the current search KListView has too many items (rows) - remove the unneeded
  while( new_rows > searchFilesListView->childCount() )
      new KListViewItem( (QListView*) searchFilesListView, 0 );
// If the search KListView's items count is low - add the needed items (rows)
  while( new_rows < searchFilesListView->childCount() )  {
      KListViewItem* t = (KListViewItem*) searchFilesListView->lastItem();
      if ( t ) delete t;
  }
//  kdDebug() << "(kmk):LVupdate: table setup time: " << ptime.elapsed() << " ms." << endl;
//  ptime.restart();

// If there are any matches at all...
  if ( new_rows )
  {
// use this var as a counter
    new_rows = 0;
// and new_item is a pointer we'll set to the item we are currently adjusting...
//  all the items we need should have been added with the previous loops
    KListViewItem * new_item = (KListViewItem*) searchFilesListView->firstChild();
// This is the exact same loop as above, but here will do something with what matches
    for( MmDataList::iterator it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
     {
       if( (!(*it).IsDir()) && ( ((*it).Folder().compare(p)==0) ||  (((*it).FileName().contains(p,FALSE)>0) ||
             ((*it).Artist().contains(p,FALSE)>0) ) || (((*it).Title().contains(p,FALSE)>0)  ||
             ((*it).Album().contains(p,FALSE)>0) ) ) )
        {
         if ( !new_item ) qFatal("slotSearchButtonClicked: supposedly \"equal\" checks are giving "
                                 "different results!");
         new_item->setText( 0, (*it).Folder() );         // folder - for use in play or enqueue file(s) action
         new_item->setText( 1, (*it).FileName() );       // file
         new_item->setText( 2, (*it).Artist() );         // artist
         new_item->setText( 3, (*it).Title() );          // title
         new_item->setText( 4, (*it).Album() );          // album
         new_item->setText( 5, (*it).Genre() );          // genre
         new_item->setText( 6, (*it).Comment() );        // comment
         new_item->setText( 7, ((*it).Year()!=0)?QString::number( (*it).Year() ):"" );      // song year
         new_item->setText( 8, ((*it).TrackNum()!=0)?QString::number( (*it).TrackNum() ):"" );  // track number
         new_item->setText( 9, formatted_string_from_seconds( (*it).Length() ) ); // length
         new_item->setText( 10, QString::number( (*it).FileSize() / 1024 ) + " kB" ); // file size
//        dt.setTime_t( (*it).ModifiedTime() );
         new_item->setText( 11, QString::number( (*it).BitRate()) );       // bitrate
         new_item->setText( 12, QString::number( (*it).SampleRate()) );    // samplerate
         new_item->setText( 13, QString::number( (*it).Channels()) );      // channels
         QFileInfo ext_info( (*it).FileName() );
         new_item->setText( 14, ext_info.extension(FALSE).upper() );       // type
         new_item->setText( 15, (*it).IsReadOnly()?"YES":"_no_" );         // read only
         new_item = (KListViewItem*) new_item->nextSibling();
         new_rows++;
        }
     }
//    kdDebug() <<  new_rows << " rows have been added." << endl;
// update table contets after manipulation
    searchFilesListView->clearSelection();
// here we update the first 5 columns' width
  Q_CHECK_PTR( s );
// honor user setting whether he/she wants auto-column-width, or likes speed more
  if ( s->autoColumnWidth() )
      for ( ushort k=0; k<7; k++) searchFilesListView->adjustColumn( k );
  }
  searchFilesListView->setEnabled( TRUE );
  if(!(new_rows)) KMessageBox::sorry( this, i18n("No files were found, that match your search criterias!"),
                                            i18n("KDE Music Kataloger") );
}

void kmk::slotClearButtonClicked()
{
  leSearchFor->clear();
  searchFilesListView->clear();
}

void kmk::slotDoneButtonClicked()
{
  filesListView->clearSelection();
  tabWidget1->setCurrentPage(0);
}



/**============================================================================================================**
 **  NON SLOTS      NON SLOTS      NON SLOTS      NON SLOTS      NON SLOTS      NON SLOTS      NON SLOTS       **
 **============================================================================================================**/

void kmk::clearCatalogData( const bool UpdateState )
{
    if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
        kdDebug() << "clearCatalogData: clearing all in-memory catalog data and stats...." << endl;
// clear catalog tree
    MusicCatalog.clear();
    treeListView->clear();
    listViewRootItem = (KListViewItem*) treeListView;
    cur_vlItem = listViewRootItem;
// clear table showing files
    filesListView->clear();
// clear table showing search result files
    searchFilesListView->clear();
// reset stats counters, locate mode helper variables
    locateMode = FALSE;   DelSelFile = "";      DelSelDir = "";
    bytes_to_read = 0;    total_bytes = 0;      total_files = 0;
    total_folders = 0;    total_play_time = 0;  average_file_size = 0;
// and finally, update state, title and actions accordingly
    if( UpdateState )  setCatalogStateAndUpdate( NoCatalog );
    if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
        kdDebug() << "clearCatalogData: ...done!" << endl;
}

void kmk::setCatalogStateAndUpdate( const kmk::CatalogStateEnum state )
{
  if( s->dbg() & KMK_DBG_OTHER )
  switch (state) {
   case kmk::NoCatalog:
     kdDebug() << "setCatalogStateAndUpdate: setting catalog state to \"NO CATALOG\"..." << endl;
     break;
   case kmk::Modified:
     kdDebug() << "setCatalogStateAndUpdate: setting catalog state to \"MODIFIED\"..." << endl;
     break;
   case kmk::Saved:
     kdDebug() << "setCatalogStateAndUpdate: setting catalog state to \"SAVED\"..." << endl;
     break;
   default:
     kdDebug() << "setCatalogStateAndUpdate: setting catalog state to \"UNSPECIFIED\"..." << endl;
     break;
  }
  CatalogState = state;
  switch (state) {
   case kmk::NoCatalog:
     catalogFileName = QDir::homeDirPath()+"/"+_KMK_NEWCATALOG;
     this->setCaption( i18n("No catalog - ") + savedCaption );
     fileSaveAction->setEnabled( FALSE );
     fileSaveAsAction->setEnabled( FALSE );
     fileCatalogFileStats->setEnabled( FALSE );
     fileCatalogFileClose->setEnabled( FALSE );
     catalogAddNewFolderAction->setEnabled( FALSE );
     TagEditAction->setEnabled( TRUE );
     listTableLocateAction->setEnabled( FALSE );
     playerEnqueueAction->setEnabled( FALSE );
     playerEnqueueDirAction->setEnabled( FALSE );
     playerEnqueueDirSubdirsAction->setEnabled( FALSE );
     playerPlayDirAction->setEnabled( FALSE );
     playerPlayDirSubdirsAction->setEnabled( FALSE );
     playerPlaySelectionAction->setEnabled( FALSE );
     main_container_widget->setEnabled( FALSE );
     break;
   case kmk::Modified:
     this->setCaption( i18n("Catalog [%1][UNSAVED] - ").arg(catalogFileName) + savedCaption );
     fileSaveAction->setEnabled( TRUE );
     fileSaveAsAction->setEnabled( TRUE );
     fileCatalogFileStats->setEnabled( TRUE );
     fileCatalogFileClose->setEnabled( TRUE );
     catalogAddNewFolderAction->setEnabled( TRUE );
     TagEditAction->setEnabled( TRUE );
     listTableLocateAction->setEnabled( TRUE );
     playerEnqueueAction->setEnabled( TRUE );
     playerEnqueueDirAction->setEnabled( TRUE );
     playerEnqueueDirSubdirsAction->setEnabled( TRUE );
     playerPlayDirAction->setEnabled( TRUE );
     playerPlayDirSubdirsAction->setEnabled( TRUE );
     playerPlaySelectionAction->setEnabled( TRUE );
     main_container_widget->setEnabled( TRUE );
     break;
   case kmk::Saved:
     this->setCaption( i18n("Catalog [%1] - ").arg(catalogFileName) + savedCaption );
     fileSaveAction->setEnabled( FALSE );
     fileSaveAsAction->setEnabled( TRUE );
     fileCatalogFileStats->setEnabled( TRUE );
     fileCatalogFileClose->setEnabled( TRUE );
     catalogAddNewFolderAction->setEnabled( TRUE );
     TagEditAction->setEnabled( TRUE );
     listTableLocateAction->setEnabled( TRUE );
     playerEnqueueAction->setEnabled( TRUE );
     playerEnqueueDirAction->setEnabled( TRUE );
     playerEnqueueDirSubdirsAction->setEnabled( TRUE );
     playerPlayDirAction->setEnabled( TRUE );
     playerPlayDirSubdirsAction->setEnabled( TRUE );
     playerPlaySelectionAction->setEnabled( TRUE );
     main_container_widget->setEnabled( TRUE );
     break;
   default:
     break;
  }
  if( s->dbg() & KMK_DBG_OTHER )
   kdDebug() << "setCatalogStateAndUpdate: done!" << endl;
}

#define UCS	"update_catalog_stats: "
void kmk::update_catalog_stats( MmDataList* list )
{
 if( ! list ) { qWarning(UCS"recieved bad list pointer!"); return; }
 if( list->isEmpty() ) { qWarning(UCS"recieved empty list!"); return; }

 total_bytes = 0;       total_files = 0;
 total_folders = 0;     total_play_time = 0;

 MmDataList::iterator mm;
 mm = list->begin();
 while ( mm != list->end() )
  {
   if( (*mm).IsDir() ) total_folders++;
   else
   {
    total_bytes += (*mm).FileSize();
    total_files++;
    total_play_time += (*mm).Length();
   }
   ++mm;
  }
 if( total_files ) average_file_size = ( total_bytes / total_files );
}

  /**
   * Remove a folder inside a MmData list and all of its children
   * (file AND folders)
   * NOTE: @p folder must NOT have the slash / symbol in the end!!!
   * @return 0        - on succes
   *         non-zero - error
   */
#define RMF "rem_MmData_Folder: "
uint kmk::rem_MmData_Folder( MmDataList* list, const QString& folder )
{
 if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
    kdDebug() << RMF"Removing [" << folder << "] and all its children from catalog..." << endl;
 MmDataList::iterator sm_mm;
 QString qq1 = folder + "/";
 tree_needs_update = FALSE;
 sm_mm = list->begin();
 while ( sm_mm != list->end() )
  {
   if ( ((*sm_mm).Folder().startsWith( qq1 ))||((*sm_mm).Folder().compare( folder )==0) )
    {
     sm_mm = list->remove( sm_mm );
     tree_needs_update = TRUE;
    }
   else ++sm_mm;
  }
 if(s->dbg() & KMK_DBG_FS_OPS )
    kdDebug() << RMF"Done removing [" << folder << "] and its children." << endl;
 if( tree_needs_update ) return 0;
 else return 1; // error - no such folder in list
}


/*          WARNING !!! OBSESIVE USE OF RECURSION!!!!
*/
#define TT	"traverse_tree: "
unsigned long kmk::traverse_tree( const QString& dir, const QString& pf )
{
 Q_UNUSED( pf );
 kdDebug() << TT"pf now shows : [" << pf << "]." << endl;
 if(kmkSmooth->elapsed() >= _KMK_UPDATE_PERIOD)
  { kmkSmooth->restart(); qApp->processEvents(); }
/* Here we mark our tree depth - subDlevel is a kmk private var */
 subDlevel++;
 if( subDlevel > 1024 )
  {
   if( s->dbg() & KMK_DBG_FS_OPS )
       kdDebug() << TT"Not scanning tree deeper than 1024 levels!" << endl;
   subDlevel--;
   return 0;
  }
 //qWarning( "looking in \""+dir+"\"...");
/* Create a QDir object, set to the "dir-for-scan"
   The filter is set to DIRECTORIEs, without symlinks     */
 QDir d ( dir );
 d.setFilter( QDir::Dirs | QDir::NoSymLinks );
/* This gets our "dir-for-scan" subdirs' in @p list        */
 const QFileInfoList *list = d.entryInfoList();
 QFileInfoListIterator it( *list );
/* @p fi is used to get the dir name and last modification time      */
 QFileInfo *fi = new QFileInfo( d.absPath() );
 QString parent_dir = d.absPath();
 if( pf.compare( "AUTO")==0 )  {
   if( subDlevel == 1 ) parent_dir = "TREE_BASE";
   else                 parent_dir.setLength( parent_dir.length() - (d.dirName().length() + 1) );
  }
 else parent_dir=pf;
 if( s->dbg() & KMK_DBG_FS_OPS )
     kdDebug() << TT"Parent_dir set to : " << parent_dir << endl;
/* Create a new MmDataList entry with the extracted above data
 for the "dir-for-scan" and set @p new_dir to point at it;
 we need a pointer to this new item, because if we add any files,
 contained in this dir, or its subdirs, we must update its
 MmFileSize field according to this below...          */
 MmDataList::iterator new_dir =
/* Some explanation : as we use the same elements to store info
 for audio files, and the dirs containing them - we do this trick:
 if the MmData item holds info on DIRECTORY - we set its fields like this:
 MmFolder      -  set to the ABSOLUTE DIR NAME
 MmFileName    -  set to the SHORT DIR NAME
 MmIsFolder    -  set to TRUE
 MmReadOnly    -  wether or not we have permission to write in DIR
 MmLength      -  set to the number of files in "dir-to-scan" subdirs
 MmFileSize    -  set to the number of files contained - updated later */
    MusicCatalog.append( MmData( d.absPath(), // folder
                                 d.dirName(), // filename
                                          "", // artist
                                          "", // title
                                          "", // album
                                          "", // genre
                                  parent_dir, // comment
                                           0, // year
                                           0, // track number
                                           0, // file size     - updated later
               fi->lastModified().toTime_t(), // modified time
                            fi->isWritable(), // read only? - we may use this to determine if we can "rename" dirs
                                        TRUE, // is dir?
                                           0, // audio duration (length) in secs - upd. later
                                           0, // channels
                                           0, // samplerate
                                           0 ) ); // bitrate
/* We are done with @p fi for now - so, get rid of it */
 delete fi;
/* Create a new KListView entry, pointed by @p ptr and if it is at
 the catalog root make its name the absolute path
 to "dir-for-scan", otherwise use short dir name;
 we set this item's parent to be what's pointed by kmk's private
 var cur_vlItem - it should point to what should be this item's
 parent in the catalog tree */
// KListViewItem *ptr;
//////////////////////// kdDebug() << "about to crash..." << endl;
// if ( subDlevel == 1 ) ptr = new KListViewItem( (QListView*) treeListView, d.absPath() );
// else ptr = new KListViewItem( (QListViewItem*) cur_vlItem, d.dirName() );
//////////////////////// kdDebug() << "Strange, did not crash...." << endl;
/* Set the new catalog tree item to be closed, non-expandable:
 if it happens to be parent of another - we will fix it later */
// ptr->setOpen( FALSE ); ptr->setExpandable( FALSE );
/* Here we initialize our subdirs' loaded files counter @p lf      */
 unsigned long lf = 0;
/* This checks if "dir-for-scan" has any subdirs         */
 if ( !list->isEmpty() )
 {
/* And if it has, we go over the list of "dir-for-scan" subdirs     */
   while ( (fi = it.current()) != 0 )
   {
/* Check if these subdirs are not the FS reserved "." and ".."
   FIX: AND that we can also access them !!!  */
    if ( (fi->isReadable()) && (fi->isExecutable()) )
     if ( (fi->fileName().compare(".")!=0) && (fi->fileName().compare("..")!=0) )
      {
/* So, we've found a valid "dir-for-scan" subdirectory.
 If you remember - we used kmk's private var cur_vlItem as a parent
 of the KListViewItem we added earlier; so we want it to be
 "preset" when we are called, don't we? Let's make sure it is! */
//                   cur_vlItem = ptr;
/* We are setup and ready to call ourselves recursievely on this one.
 Because at the end of traverse_tree we provide a count of the
 actually added files to the catalog, including the added files
 from each subdir, lets increase @p lf with the numer of files in
 the directory we want to check...
 Here is the tracking :      */
                   lf += traverse_tree( d.filePath( fi->fileName() ) );
      }
/* Then go to the next in the list */
    ++it;
   }
/* We finished calling ourselves recursievly. So, if the subdirs'
 scan added any files - set the new catalog item to represent this
 accordingly - we hold subentries, so we need to be expandable;
 Maybe the user could set wheter or not the new catalog is already
 open or collapsed     */
//   if ( lf > 0 ) {  ptr->setOpen( FALSE );  ptr->setExpandable( TRUE ); }
   //qWarning("having "+QString::number(lf)+" files reported back...");
  }
/* Then we set our "dir-for-scan" filter to files, without symlinks */
 d.setFilter( QDir::Files | QDir::NoSymLinks );
/* And get a list of its contents, after the filtering is apllied */
 const QFileInfoList *list2 = d.entryInfoList();
 QFileInfoListIterator it2( *list2 );
 QFileInfo *fi2;
/* These we will use for storage of the exctracted data from files */
 QString art, ttl, alb, gen, cmt;
/* This will be our "dir-for-scan" ONLY (i.e. without including
 the count from the subdirs) loaded files counter; zero-init it.
 Nowadays there are file systems, capable of handling enormous amounts
 of files in a single directory - make sure we don't choke on someones
 huge collection, by using extra large counter.*/
 unsigned long loaded_files = 0;
/* A cycle to iterate over the files in "dir-for-scan" */
 while ( (fi2 = it2.current()) != 0 ) 
 {
  if(kmkSmooth->elapsed() >= _KMK_UPDATE_PERIOD)
   { kmkSmooth->restart(); qApp->processEvents(); }
/* If the files aren't with the supported extensions - ignore them;
 We call QFileInfo->extension(FALSE) because we need only chars after LAST '.' */
  if ( fi2->isReadable() )
     // LOSSLESS first
   if ( ( fi2->extension(FALSE).lower().compare("wv" )  == 0 )   || // wav-pack
        ( fi2->extension(FALSE).lower().compare("tta" ) == 0 )   || // true-audio
        ( fi2->extension(FALSE).lower().compare("ape" ) == 0 )   || // monkey's audio
/* Is it correct for a FLAC file to have any other extension than "flac"?
   For example - can it be named "my_audio_file.fla"? Ideas, advices.... */
        ( fi2->extension(FALSE).lower().compare("flac") == 0 )   ||   // flac
     // LOSSY next
        ( fi2->extension(FALSE).lower().compare("mp3")  == 0 )   || // mp3
        ( fi2->extension(FALSE).lower().compare("ogg")  == 0 )   || // ogg-vorbis
        ( fi2->extension(FALSE).lower().compare("spx")  == 0 )   || // ogg-speex
        ( fi2->extension(FALSE).lower().compare("mpc")  == 0 )    ) // muse-pack
    {
/* Init our storage for this file... */
      art = "";  ttl = "";  alb = "";  gen = ""; cmt = "";
      using namespace TagLib;
/* Use TagLib's functions to extract meta data and audio properties;
 This tells TagLib to read tags, and be as fast as possible */
      if( s->dbg() & KMK_DBG_FS_OPS )
        kdDebug() << TT"Now checking: " << d.filePath( fi2->fileName().latin1() ) << endl;
      TagLib::FileRef f( d.filePath( fi2->fileName().latin1() ), TRUE, TagLib::AudioProperties::Fast );
/* If TagLib found the file to be valid - then add it to the catalog.
 Maybe here would be a good place to update some global catalog stats,
 like total storage used by the collection we are cataloguining, number
 of files in it, average bitrate, highest one, lowest one, biggest file,
 smallest file... and anything else someone might consider "interesting"!*/
      if( f.file()->isValid() )
      {
       TagLib::String s;
       s = f.tag()->artist();    art = s.toCString();
       s = f.tag()->title();     ttl = s.toCString();
       s = f.tag()->album();     alb = s.toCString();
       s = f.tag()->genre();     gen = s.toCString();
       s = f.tag()->comment();   cmt = s.toCString();
/* Some of the extracted file data is in our storage vars now.
 Create a new MmData item, and fill it with what we've read. */
       MmData ni = MmData();
       ni.setFolder( d.absPath() );
       ni.setFileName( fi2->fileName() );
       ni.setArtist( art );
       ni.setTitle( ttl );
       ni.setAlbum( alb );
       ni.setGenre( gen );
       ni.setComment( cmt );
       ni.setYear( f.tag()->year() );
       ni.setTrackNum( f.tag()->track() );
       ni.setFileSize( fi2->size() );
//      qDebug( "To "+QString::number( total_bytes )+" B, adding "+QString::number((Q_ULLONG) fi2->size())+" B." );
       total_bytes += (Q_ULLONG) fi2->size();
       ni.setModifiedTime( fi2->lastModified().toTime_t() );
/* How about using Qt's functions QFile and QDir to check this?
 Not a problem! I've checked it - TagLib gives correct data; in a bunch of mp3s, I
 set to 2 of them to have only read acces - TagLib said exactly this! The only thing
 left in mind is speed, but... how do I check IT?                 Pl.Petrov */
       ni.setIsReadOnly( f.file()->readOnly() );
       ni.setIsDir( FALSE ); // we are adding a file
       ni.setLength( f.audioProperties()->length() ); total_play_time += ni.Length();
       ni.setChannels( f.audioProperties()->channels() );
       ni.setSampleRate( f.audioProperties()->sampleRate() );
       ni.setBitRate( f.audioProperties()->bitrate() );
/* Now, after we've filled our new catalog item - lets add it
 to the catalog list    */
       MusicCatalog.append( ni );
/* We said we will keep track of loaded files number - do so */
       loaded_files++;   total_files++;
      }
    }
/* Go to the next file in "dir-for-scan"... */
   ++it2;
 }
/* Now here is what we've got so far:
   o) new_dir - it is a pointer to the MmData item,
                containing the "dir-for-scan" directory info;
   o) ptr - a pointer the KListViewItem, which we created and added to the catalog tree;
   o) lf - contains the number of files added from all "dir-for-scan" SUBDIRs;
   o) loaded_files - contains the number of files added from "dir-for-scan" itself;
 If any of the last two in the list above is non-zero...     */
 if ( loaded_files || lf )
/* ...we must update the MmData item, containing the info for "dir-for-scan" to comply with this:
  [ ...if a MmData item holds info for a directory,
    its MmFileSize field holds a count of the files contained
    in it, !!! NOT !!! including its subdirs.                  ] 
  We also update our overall folder counter.*/
 {   (*new_dir).setFileSize( loaded_files );   (*new_dir).setLength( lf );  total_folders++;
     tree_needs_update = TRUE;  }
/* If the sum of the last two is zero (0), or... lets put it this way -
 if both of them are zeros - then the first two ought to be removed; */
 else  {   MusicCatalog.remove( new_dir );  /*  delete( ptr ); */      }
/* We are done on this level - return subDlevel to where it was... */
 subDlevel--;
 if( (subDlevel == 0) && (s->dbg() & KMK_DBG_FS_OPS ) )
      kdDebug() << TT"Finishing MAIN traverse_tree..." << endl;
/* And then return the sum of files in "dir-for-scan" and the files in
 its subdirectories.*/
 return (loaded_files + lf);
}

  /**
   * This private function starts by clearing current contents of
   * Tree KListView; next it goes trough kmk's MusicCatalog, recreating
   * the tree, described in the MmData structure, inside the Tree view
   */
void kmk::update_tree_view()
{
  long dirs = 0;
  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
      kdDebug() << "update_tree_view: starting..." << endl;
  if( MusicCatalog.isEmpty() )
   {
    if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
      kdDebug() << "update_tree_view: sorry, no data in catalog!" << endl;
    return;
   }
// clear current contents of treeListView
  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
      kdDebug() << "update_tree_view: clearing contents of treeListView...." << endl;
  treeListView->clear();
  listViewRootItem = (KListViewItem*) treeListView;
  cur_vlItem = listViewRootItem;

  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
      kdDebug() << "update_tree_view: cleared contents of treeListView!" << endl;
// go through MusicCatalog...
  MmDataList::iterator it;
  KListViewItem *ptr;

  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
   {
    kdDebug()<<"******* SHOWING MUSIC CATALOG DATA SEQUENTLY ********"<<endl;
    for( it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
     {
      kdDebug()<<"FileName(): "<<(*it).FileName()<<"  "
                   "Folder(): "<<(*it).Folder()<<endl;
      kdDebug()<<"Comment(): "<<(*it).Comment()<<"  "
                   "IsDir(): "<<(*it).IsDir()<<endl;
      kdDebug()<<"---------------------------"<<endl;
     }
    kdDebug()<<"******** DONE MUSIC CATALOG DATA *************"<<endl;
   }

  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
      kdDebug() << "update_tree_view: setting all dirs to [NOT ADDED]..." << endl;
// Go trough MusicCatalog and for each dir, set its BitRate() to 22
// where non-zero value means that the dir is NOT in tree view and
// and should be added at the correct place
  for ( it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
       if ( (*it).IsDir() ) { (*it).setBitRate( 22 ); dirs++; }

  for ( it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
   {
// look for a dir, that is NOT ADDED
     if ( (*it).IsDir() && ( (*it).BitRate() > 0 ) )
      {
// when found one, if its parent is "TREE_BASE" - add it to the top...
        if ( (*it).Comment().compare( "TREE_BASE" )==0 )
         {
          if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
            kdDebug() << "update_tree_view: adding " << (*it).Folder()
                      << " as a top-level TREE item..." << endl;
          ptr = new KListViewItem( (QListView*) treeListView, (*it).Folder() );
//   and reflect the fact that its added to tree view
          cur_vlItem = ptr;
          (*it).setBitRate( 0 );
          dirs--;
          it = MusicCatalog.begin();
         }
// and if its not - find the location where the new one has to be added
        else
         {
// just for speed's sake....
           bool added = FALSE;
           while( cur_vlItem != listViewRootItem )
            {
             QListViewItem* lv = cur_vlItem;
      // this one will hold the full-blown path to the last added folder
             QString folder_name = QString::null;
      // this loop extracts a full path of the selected dir in the Tree view by
      // going from it to the top, collecting data on the way in *folder_name*
             while ( TRUE ) {
               folder_name.prepend( lv->text(0) );
               if( lv->parent() )
                {
                  if( lv->parent()->text(0).compare("/") != 0 ) folder_name.prepend( "/" );
                  lv = lv->parent();
                }
      // when the top is reached - the loop "break"s
               else break;
             }
             if( (*it).Comment().compare( folder_name )==0 )
              {
               if( s->dbg() & (KMK_DBG_CAT_MEM_OPS | KMK_DBG_OTHER ) )
                   kdDebug() << "update_tree_view: FAST adding " << (*it).FileName() << "." << endl;
      // and if it is - add it as a child of trit
               ptr = new KListViewItem( (QListViewItem*) cur_vlItem, (*it).FileName() );
      // and reflect the fact that its added to tree view
               cur_vlItem = ptr;
               (*it).setBitRate( 0 );
               dirs--;
               it = MusicCatalog.begin();
               added = TRUE;
               break;
              }
             else if( cur_vlItem->parent() ) cur_vlItem = (KListViewItem*) cur_vlItem->parent(); else break;
            }
           if( added ) continue;
           else if( s->dbg() & (KMK_DBG_CAT_MEM_OPS | KMK_DBG_OTHER ) )
                    kdDebug() << "update_tree_view: doing a slow search..." << endl;
           QListViewItemIterator trit( (QListView*) treeListView );
           while ( trit.current() )
            {
             QListViewItem* lv = trit.current();
      // this one will hold the full-blown path to the last added folder
             QString folder_name = QString::null;
      // this loop extracts a full path of the selected dir in the Tree view by
      // going from it to the top, collecting data on the way in *folder_name*
             while ( TRUE ) {
               folder_name.prepend( lv->text(0) );
               if( lv->parent() )
                {
                  if( lv->parent()->text(0).compare("/") != 0 ) folder_name.prepend( "/" );
                  lv = lv->parent();
                }
      // when the top is reached - the loop "break"s
               else break;
             }
             if( (*it).Comment().compare( folder_name )==0 )
              {
               if( s->dbg() & (KMK_DBG_CAT_MEM_OPS | KMK_DBG_OTHER ) )
                   kdDebug() << "update_tree_view: adding " << (*it).FileName() << "." << endl;
      // and if it is - add it as a child of trit
               ptr = new KListViewItem( (QListViewItem*) trit.current(), (*it).FileName() );
      // and reflect the fact that its added to tree view
               cur_vlItem = ptr;
               (*it).setBitRate( 0 );
               dirs--;
               it = MusicCatalog.begin();
               break;
              }
             ++trit;
            }
         }
      }
   }
// check if we added all the dirs...
  if( dirs ) qWarning("Possible catalog incosistency detected!!! Better Re-Scan!!!");
  if( dirs && ( s->dbg() & KMK_DBG_CAT_MEM_OPS ) )
      kdDebug() << "update_tree_view: Possible catalog incosistency detected!!! Better Re-Scan!!!" << endl;
// ok, we are done
  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
      kdDebug() << "update_tree_view: done updating treeListView !" << endl;
}


/*          WARNING !!! OBSESIVE USE OF RECURSION!!!!
*/
void kmk::bytes_to_read_by_traverse( const QString& dir )
{
 if(kmkSmooth->elapsed() >= _KMK_UPDATE_PERIOD)
  { kmkSmooth->restart(); qApp->processEvents(); }
// kdDebug() << "called with param " << dir << endl;
 QDir d ( dir );                                d.setFilter( QDir::Dirs | QDir::NoSymLinks );
 const QFileInfoList *list = d.entryInfoList(); QFileInfoListIterator it( *list );
 QFileInfo *fi = new QFileInfo();
 if ( !list->isEmpty() )
  while ( (fi = it.current()) != 0 )
  {
    if ( (fi->isReadable()) && (fi->isExecutable()) )
     if ( (fi->fileName().compare(".")!=0) && (fi->fileName().compare("..")!=0) )
      {
       if ( !fi->isDir() ) kdDebug() << "...and we are trying to do dir op on non-dir!" << endl;
       else                bytes_to_read_by_traverse( d.filePath( fi->fileName() ) );
      }
    ++it;
  }
 d.setFilter( QDir::Files | QDir::NoSymLinks );        const QFileInfoList *list2 = d.entryInfoList();
 QFileInfoListIterator it2( *list2 );                        QFileInfo *fi2;
 while ( (fi2 = it2.current()) != 0 ) 
 {
   if(kmkSmooth->elapsed() >= _KMK_UPDATE_PERIOD)
    { kmkSmooth->restart(); qApp->processEvents(); }
   if ( fi2->isReadable() )
    if ( ( fi2->extension(FALSE).lower().compare("mp3")  == 0 )   ||
         ( fi2->extension(FALSE).lower().compare("ogg")  == 0 )   ||
         ( fi2->extension(FALSE).lower().compare("flac") == 0 )     )
     {  bytes_to_read += (Q_ULLONG) fi2->size();  files_to_read++; }
   ++it2;
 }
}

/* Some neat code I am really proud of: fast and effective; doesn't tollerate storage errors, though... */
uint kmk::generateListViewSubtreeXML( const KListViewItem *item, QDomDocument doc, QDomElement e, const uint add_lv )
{
  if( item != 0 )
  {
//    kdDebug() << "DOMelem  <tree_item Name=\"" << item->text(0) <<"\" M=\"" << add_lv << "\" />" << endl;

    QDomElement dscr =
       doc.createElement( "FolderTreeDescriptor" );
    dscr.setAttribute( "FolderName",  item->text(0) );
    dscr.setAttribute( "GoUpHowMuch", add_lv );
    e.appendChild( dscr );

    uint levels_added=0;
    KListViewItem * some_child = (KListViewItem*) item->firstChild();
    while( some_child )
     {
      levels_added = generateListViewSubtreeXML( some_child, doc, e, levels_added );
      some_child = (KListViewItem*) some_child->nextSibling();
     }

    return ++levels_added;
  }
  else return 0;
}

void kmk::generateListViewXML( const KListView *list, QDomDocument doc, QDomElement e )
{
  if( list != 0 )
  {
    uint levels_added=0;
    KListViewItem * some_child = (KListViewItem*) list->firstChild();
    while( some_child )
     {
      levels_added = generateListViewSubtreeXML( some_child, doc, e, levels_added );
      some_child = (KListViewItem*) some_child->nextSibling();
     }
  }
}


/**
 *  Finds all files in TreeList currentItem and all its subfolders
 *  and passes them to player_bin with parameter @param act
 *  NOTE this function respects _kmk_include_subdirs
 *  NOTE 2: in a rethink of what the function should do,
 *  the new behaviour is that it writes all the relevant files
 *  in a .M3U or .PLS playlist file (stored with a randomly generated
 *  name in /tmp/kmk) and passes it to the player with @p act
 */
void kmk::playerDir( uint act )        //    act>=1 - play;   act==0 - enqueue
{
  QListViewItem* lv = treeListView->currentItem();
  if ( lv != 0 )
    {
// generate some random based filename for the playlist file
     QString flNm = "/tmp/kmk/kmk_lst_"+QString::number( random() )+".m3u";
     QFile file( flNm );
     if ( file.open( IO_WriteOnly ) )
      {
       unsigned long files_cnt = 0;
       QTextStream stream( &file );
       stream <<  "#EXTM3U\n";
// determine the folder we use as root and save it in @p d
       QString d;
       while ( TRUE ) {
           d.prepend( lv->text(0) );
           if( lv->parent() ) { if( lv->parent()->text(0).compare("/") != 0 ) d.prepend( "/" ); lv = lv->parent(); }
           else break;
       }
// do the acctual adding of files to the playlist....
       MmDataList::iterator it;
       for ( it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
        if ( _kmk_include_subdirs )
         {
          if ( ( !((*it).IsDir()) ) && ( (( (*it).Folder().compare(d)==0 )) || ( (*it).Folder().contains(d+"/",TRUE)==1 ) ) )
           {
            stream <<  "#EXTINF:" << (*it).Length() << " ," << (*it).Artist() << " - " << (*it).Title() << "\n";
            stream <<  (*it).Folder()+"/"+(*it).FileName()  << "\n";
            files_cnt++;
           }
         }
        else
         {
          if ( ( !((*it).IsDir()) ) && ( (*it).Folder().compare(d)==0 ) )
           {
            stream <<  "#EXTINF:" << (*it).Length() << " ," << (*it).Artist() << " - " << (*it).Title() << "\n";
            stream <<  (*it).Folder()+"/"+(*it).FileName()<<"\n";
            files_cnt++;
           }
         }
        file.close();

// if we added ANY files, then AND ONLY THEN use the external player
        if( files_cnt )
          if (act)   player->playPlaylist( flNm );
          else       player->addPlaylist( flNm );
//        file.remove();
       }
      else QMessageBox::warning( this, i18n("KDE Music Kataloger"),i18n("Could not open %1 for writing!").arg(flNm),
           QMessageBox::Abort,QMessageBox::NoButton,QMessageBox::NoButton );
     }
}

MmData* kmk::findMmData( const QString& folder, const QString& filename )
{
  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
      kdDebug() << "findMmData: looking for [" << folder << "], [" << filename << "]..." << endl;
  bool found = FALSE;
  MmDataList::iterator it;
  for( it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
   {
     if( ((*it).Folder().compare(folder)==0) && ((*it).FileName().compare(filename)==0) )
      {
        found = TRUE;
        if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
          kdDebug() << "findMmData: found. " << endl;
        break;
      }
   }
  if( found ) return &(*it);
  else return 0;
}

const QString kmk::formatted_string_from_seconds( const Q_ULLONG t )
{
  QString ttime = "";
  unsigned short secs, mins, hours, years;
  secs = mins = hours = years = 0;
  uint days = 0;

  if ( t == 0 ) return ttime;
  years = t / 30758400;
  days = (t - (years * 30758400)) / 86400;
  hours = (t - ((years * 30758400) + (days * 86400))) / 3600;
  mins = (t - ((years * 30758400) + (days * 86400) + (hours * 3600))) / 60;
  secs = t % 60;
  if ( years ) if ( years > 1 ) ttime.append( i18n("%1 years").arg(years) );
               else ttime.append( i18n("1 year") );
  if ( days )
   {
    if ( years ) ttime.append(", ");
    if ( days > 1 ) ttime.append( i18n("%1 days").arg(days) );
    else ttime.append( i18n("1 day") );
   }
  if ( (days || years) && (hours || mins || secs) ) ttime.append(", ");
  if ( hours )
  {
   if ( hours>9 ) ttime.append( QString("%1:").arg(hours) );
   else ttime.append( QString("0%1:").arg(hours) );
  }
  if ( secs || mins || hours )
  {
   if ( mins>9 ) ttime.append( QString("%1:").arg(mins) );
   else ttime.append( QString("0%1:").arg(mins) );
  }
  if ( secs || mins || hours )
  {
   if ( secs>9 ) ttime.append( QString("%1").arg(secs) );
   else ttime.append( QString("0%1").arg(secs) );
  }
  return ttime;
}

const bool kmk::catalog_has_dir( const QString & looked_for )
{
  bool found = FALSE;
  MmDataList::iterator it;

  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
    kdDebug() << "catalog_has_dir: Now will look for dir [" << looked_for << "]..." << endl;
// QDir gives us paths ending on "/" and in the catalog - folders don't
// end on "/"; so, strip that last slash "/" symbol
  QString stripped_looked_for = looked_for;
  if ( stripped_looked_for.length()>1 )
       stripped_looked_for.setLength( stripped_looked_for.length() - 1 );
  for ( it = MusicCatalog.begin(); it != MusicCatalog.end(); ++it )
   {
     if ( (*it).IsDir() )
      {
        if ( (*it).Folder().compare( stripped_looked_for )==0 )
        {
          found = TRUE;
          if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
            kdDebug() << "catalog_has_dir: ...found! " << endl;
          break;
        }
      }
   }

  return found;
}

void kmk::loadCatalog( const QString & fileName )
{
    QDomDocument doc = QDomDocument::QDomDocument();
    QFile file( fileName );
    if ( !file.open( IO_ReadOnly | IO_Raw ) ) {
       QMessageBox::warning( this, i18n("KDE Music Kataloger"),
                                   i18n("Could not open %1 for reading!").arg(fileName),
                                   QMessageBox::Abort,QMessageBox::NoButton,QMessageBox::NoButton );
       return;
     }
    this->setCaption( i18n("Checking integrity of [%1] - ").arg(fileName) );
    this->repaint(); qApp->processEvents();
//    sleep( 5 ); kdDebug() << " sleeping..." << endl;
    QString xml_parse_err = "test"; int err_ln = 0; int err_cl = 0;
    QTime parse_time; parse_time.start();
    if ( !doc.setContent( &file, TRUE, &xml_parse_err, &err_ln, &err_cl ) ) {
      file.close();
//      qWarning( QString::number( *err_ln )+ " " + QString::number( *err_cl ) );
      QMessageBox::warning( this, i18n("KDE Music Kataloger"),
             i18n("Error parsing catalog file %1 !\n MSG: %2 on line %3, column %4.")
       .arg(fileName).arg(xml_parse_err.ascii()).arg(QString::number( err_ln )).arg(QString::number( err_cl )),
         QMessageBox::Abort,QMessageBox::NoButton,QMessageBox::NoButton );
      this->setCaption( savedCaption );
      return;
    }
    file.close();
    if( s->dbg() & KMK_DBG_CATALOG_IO )
      kdDebug() << "loadCatalog: parse_file(SAX2 parse): elapsed time: " 
                << parse_time.elapsed() << " ms." << endl;
    parse_time.restart();
    clearCatalogData();
    this->setCaption( i18n("Loading [%1]...").arg(fileName) );
    catalogFileName = fileName;
    qApp->processEvents();

/** READ CATALOG VERSION INFO - if none is found - inform, and bail out */
    bool process = TRUE;
    ulong nodes_to_add = 0;
    QDomNodeList ml = doc.elementsByTagName( "KMK_catalog_file_data" ); QDomNode n; QDomElement e; QDomAttr a;
    for ( uint i = 0; i<ml.count(); i++)
     {
       n = ml.item( i );
       if ( n.isElement() )
        {  e = n.toElement();
          QString tx = e.attribute("Version");
          if( !tx.isNull() )
           {
             if( tx.toInt() > 1 ) KMessageBox::sorry( this, i18n("The catalog [%1] is saved in new format (v.%2),"
             "so some data will not be recognized.").arg(fileName).arg(tx.toInt()),i18n("KDE Music Kataloger") );
             if( tx.toInt() == 0 ) process = FALSE;
           }
        }
     }
    if( s->dbg() & KMK_DBG_CATALOG_IO )
        kdDebug() << "loadCatalog: parse_file(new read): catalog version read time: "
                  << parse_time.elapsed() << " ms." << endl;
    parse_time.restart();
    if( process )
     {
      ml = doc.elementsByTagName( "CatalogFile_Statistics" );
      for ( uint i = 0; i<ml.count(); i++)
       {
         n = ml.item( i );
         if ( n.isElement() )
          { e = n.toElement();
            QString tx = e.attribute("Total_files_count");
            if( !tx.isNull() )   total_files = (Q_ULLONG) tx.toDouble();
            tx = e.attribute("Total_bytes_count");
            if( !tx.isNull() )   total_bytes = (Q_ULLONG) tx.toDouble();
            tx = e.attribute("Total_dirs_count");
            if( !tx.isNull() )   total_folders = (Q_ULLONG) tx.toDouble();
            tx = e.attribute("Total_secs_count");
            if( !tx.isNull() )   total_play_time = (Q_ULLONG) tx.toDouble();
            tx = e.attribute("Average_file_size");
            if( !tx.isNull() )    average_file_size = (Q_ULLONG) tx.toDouble();
          }
       }
      if( s->dbg() & KMK_DBG_CATALOG_IO )
         kdDebug() << "loadCatalog: parse_file(new read): catalog stats read time: "
                   << parse_time.elapsed() << " ms." << endl;
      parse_time.restart();
     }

/** READ THE FOLDERS STRUCTURE AUXILARY DATA - version, counters, etc */
/* Currently - IRRELEVANT
    ml = doc.elementsByTagName( "CatalogFile_TreeDescription" );
    for ( uint i = 0; i<ml.count(); i++)
     {
        n = ml.item( i );
        if ( n.isAttr() )
        { a = n.toAttr();
#ifdef __KMK_DEBUG
         kdDebug() << "(kmk): i="<<i<<"; found attr; name: "<< a.name()<<"; value: "<< a.value() << endl;
#endif
        }
        if ( n.isElement() )
        { e = n.toElement();
#ifdef __KMK_DEBUG
          kdDebug() << "(kmk): i="<<i<<"; found elem; tag: "<< e.tagName() << endl;
#endif
        }
     }
*/

/** READ THE FOLDERS STRUCTURE AND RECREATE IT IN KLISTVIEW */
/*    if( process )
     {
      KListViewItem *CURRENT = 0;  bool warned = FALSE;
      ml = doc.elementsByTagName( "FolderTreeDescriptor" );
      for ( uint i = 0; i<ml.count(); i++)
       {
          n = ml.item( i );
          if ( n.isElement() )
          { e = n.toElement();
            QString nm = e.attribute("FolderName");
            if( nm.isNull() ) kdDebug() << "READ A NULL FOLDER NAME!" << endl;
            QString up = e.attribute("GoUpHowMuch");
            if( up.isNull() ) kdDebug() << "READ A NULL GO_UP_HOW_MUCH!"<< endl;
            uint t = up.toUInt();
            while( (t) && (CURRENT) )
             {
                CURRENT = (KListViewItem*)CURRENT->parent();
                t--;
             }
            if( t && (!warned) ) { KMessageBox::sorry( this,
             i18n("The catalog file you are trying to load is messed up. "
                  "You get this message, so it passed XML sanity checks. That means almost for sure "
                  "that it has been edited by hand - you are better off if you recreate it via Catalog->New. "
                  "The scanning is fast enough anyway (around 10GB mp3 files scanned per minute)!\n"
                  "You have been warned - don't complain if something goes wrong."),
             i18n("KDE Music Kataloger") );
                  warned = TRUE; }
            if( s->dbg() & KMK_DBG_OTHER )
             if(CURRENT)
               kdDebug() << "loadCatalog: " << i << ":(t="<<t<<") adding "<<nm<<" at "<<CURRENT->text(0)<<endl;
             else
               kdDebug() << "loadCatalog: " << i << ":(t="<<t<<") adding "<<nm<<" at ROOT."<<endl;
            if(CURRENT) CURRENT = new KListViewItem( (QListViewItem*) CURRENT, nm );
            else CURRENT = new KListViewItem( (QListView*) treeListView, nm );
//kdDebug() << i << ":-------------> "<<CURRENT->text(0)<<"'s parent() is: " << CURRENT->parent() << endl;
          }
       }
      if( s->dbg() & KMK_DBG_CATALOG_IO )
        kdDebug() << "loadCatalog: parse_file(new read): folder tree reconstruction time: "
                  << parse_time.elapsed() << " ms." << endl;
      parse_time.restart();
      qApp->processEvents();
     }
// 10 February 2008 - removed folder tree reconstruction;
// whoever needs it, now should call update_tree_view() instead
*/
/** READ CATALOG ITEMS AUXILARY DATA - objects number, size, etc */
    if( process )
     {
      ml = doc.elementsByTagName( "CatalogFile_Objects" );
      for ( uint i = 0; i<ml.count(); i++)
       {
          n = ml.item( i );
          if ( n.isElement() )
          { e = n.toElement();
            QString tx = e.attribute("Total_MObjs_count");
            if( !tx.isNull() ) nodes_to_add = tx.toULong();
          }
       }
      if( s->dbg() & KMK_DBG_CATALOG_IO )
        kdDebug() << "loadCatalog: parse_file(new read): catalog objects count read time: "
                  << parse_time.elapsed() << " ms." << endl;
      parse_time.restart();
     }

// some vars used to convert old dirs data to new - fill in Comment field of MmData
    QString base_dir = "";
    long dirs_found = 0;
/** READ ACTUAL AUDIO FILE DATA INTO MusicCatalog */
    if( process )
     {
      ml = doc.elementsByTagName( "MObj" ); MmData node; uint tags_found;
      for ( uint i = 0; i<ml.count(); i++)
       {
        tags_found = 0;
        n = ml.item( i );
        if ( n.isElement() )
         {             e = n.toElement();                        QString
          tx = e.attribute("Folder");       if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setFolder( tx ); tags_found++; }
          tx = e.attribute("Filename");     if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setFileName( tx ); tags_found++; }
          tx = e.attribute("Artist");       if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setArtist( tx ); tags_found++; }
          tx = e.attribute("Title");        if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setTitle( tx ); tags_found++; }
          tx = e.attribute("Album");        if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setAlbum( tx ); tags_found++; }
          tx = e.attribute("Genre");        if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setGenre( tx ); tags_found++; }
          tx = e.attribute("Comment");      if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setComment( tx ); tags_found++; }
          tx = e.attribute("Year");         if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setYear( tx.toInt() ); tags_found++; }
          tx = e.attribute("TrackNumber");  if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setTrackNum( tx.toInt() ); tags_found++; }
          tx = e.attribute("Length");       if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setLength( tx.toInt() ); tags_found++; }
          tx = e.attribute("Modified");     if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setModifiedTime( tx.toInt() ); tags_found++; }
          tx = e.attribute("Size");         if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setFileSize( tx.toInt() ); tags_found++; }
          tx = e.attribute("Channels");     if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setChannels( tx.toInt() ); tags_found++; }
          tx = e.attribute("BitRate");      if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setBitRate( tx.toInt() ); tags_found++; }
          tx = e.attribute("SampleRate");   if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setSampleRate( tx.toInt() ); tags_found++; }
          tx = e.attribute("IsReadOnly");   if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setIsReadOnly( (tx.compare("YES")==0) ? TRUE:FALSE ); tags_found++; }
          tx = e.attribute("IsFolder");     if( tx.isNull() ) kdDebug() << "READ NULL XML ATTRIBUTE!" << endl;
          else {  node.setIsDir( (tx.compare("YES")==0) ? TRUE:FALSE ); tags_found++; }
         }
        if ( tags_found )
          {
           qApp->processEvents();
           if( node.IsDir() )
            if( node.Comment().isEmpty() )
             {
              if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
                  kdDebug() << "loadCatalog: converting old-format folder "
                            << node.Folder() << "..." << endl;
              dirs_found++;
              if( dirs_found == 1)
               {
                base_dir = node.Folder();
                node.setComment( "TREE_BASE" );
                if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
                    kdDebug() << "loadCatalog: adding folder comment [" << base_dir
                              << "]; setting it as BASE_DIR..." << endl;
               }
              else
               {
                if( node.Folder().contains( base_dir ) > 0 )
                 {
                  QString tmpQ = node.Folder();
                  tmpQ.setLength( node.Folder().length() - ( node.FileName().length() + 1 ) );
                  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
                      kdDebug() << "loadCatalog: adding folder comment [" << tmpQ
                                << "] as part of conversion..." << endl;
                  node.setComment( tmpQ );
                 }
                else
                 {
                  base_dir = node.Folder();
                  node.setComment( "TREE_BASE" );
                  if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
                      kdDebug() << "loadCatalog: adding folder comment [" << base_dir
                                << "]; setting it as NEW BASE_DIR..." << endl;
                 }
               }
             }
           MusicCatalog.append( node );
           if( s->dbg() & KMK_DBG_OTHER )
               kdDebug() << "loadCatalog: parse_file(new read): just added " << i << "-th object;" << endl;
          }
       }
      if( s->dbg() & KMK_DBG_CATALOG_IO )
        kdDebug() << "loadCatalog: parse_file(new read): node read and add time: "
                  << parse_time.elapsed() << " ms." << endl;
      parse_time.restart();
     }

    if( ! process )
      KMessageBox::sorry( this, i18n("No catalog markings found in [%1].\n"
                                     "This can happen if the file is corrupt.")
                          .arg(catalogFileName),i18n("KDE Music Kataloger") );
    else {
       if( dirs_found ) setCatalogStateAndUpdate( Modified );
       else             setCatalogStateAndUpdate( Saved );
       fileCatalogFileStats->setEnabled( TRUE );
       fileCatalogFileClose->setEnabled( TRUE );
     }
}


#include "kmk.moc"
