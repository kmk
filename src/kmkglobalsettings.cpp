/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* This is a modified version of K3B's k3bglobalsettings.cpp to suit KMK's needs*/

#include "kmkglobalsettings.h"

#include <kconfig.h>


KmkGlobalSettings::KmkGlobalSettings()
   : g_dbg(0),
     g_save_on_navigate(TRUE),
     g_auto_column_width(FALSE),
     g_load_last(TRUE),
     g_last_catalog_used("")
{
}

void KmkGlobalSettings::readSettings( KConfig* c )
{
  QString lastG = c->group();
  c->setGroup( "General Options" );

  g_dbg = c->readUnsignedNumEntry( "debug_level", 0 );
  g_save_on_navigate = c->readBoolEntry( "save_on_navigate", TRUE );
  g_auto_column_width = c->readBoolEntry( "auto_column_width", FALSE );
  g_load_last = c->readBoolEntry( "load_last", TRUE );
  g_save_geo = c->readBoolEntry( "save_geometry", TRUE );
  g_last_catalog_used = c->readEntry( "last_catalog_used", 0 );
  g_ext_player = c->readUnsignedNumEntry( "external_player_code", 0 );

  c->setGroup( lastG );
}


void KmkGlobalSettings::saveSettings( KConfig* c )
{
  QString lastG = c->group();
  c->setGroup( "General Options" );

  c->writeEntry( "debug_level", g_dbg );
  c->writeEntry( "save_on_navigate", g_save_on_navigate );
  c->writeEntry( "auto_column_width", g_auto_column_width );
  c->writeEntry( "load_last", g_load_last );
  c->writeEntry( "save_geometry", g_save_geo );
  c->writeEntry( "last_catalog_used", g_last_catalog_used );
  c->writeEntry( "external_player_code", g_ext_player );
  c->sync();

  c->setGroup( lastG );
}
