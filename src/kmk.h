/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _KMK_H_
#define _KMK_H_
#undef _KMK_DEBUG__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <kmainwindow.h>
#include <kaction.h>
//#include <kfiledialog.h>
#include <ktoolbar.h>
#include <klistview.h>
#include <klineedit.h>
#include <kdebug.h>
//#include <qvaluelist.h>
#include <qwidget.h>
#include <qvariant.h>
#include <qlayout.h>
#include <qsplitter.h>
#include <qtimer.h>
#include <qdatetime.h>
#include <qdragobject.h>

#include "kmk_progress_disp.h"
#include "mmdata.h"
#include "kmkglobalsettings.h"
#include "kmkexternalplayer.h"

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QTabWidget;
class QSplitter;
class KListView;
class QListViewItem;
class QPushButton;
class QFrame;
class QLabel;
class KLineEdit;
class KListView;

class kmk_progress_disp;

class kmk_tree_KListView : public KListView
{
Q_OBJECT
public:
    kmk_tree_KListView (QWidget *parent = 0, const char *name = 0)
    : KListView ( parent, name )
    {}
    virtual ~kmk_tree_KListView()
    {}
protected:
    virtual QDragObject *dragObject()
    {
      QListViewItem* lv = this->currentItem();
      if ( !lv ) return 0;
      QStringList l;
      QString p;

      while ( TRUE ) {
         p.prepend( lv->text(0) );
         if( lv->parent() ) { p.prepend( "/" ); lv = lv->parent(); }
         else break;
      }

      l << p;

      QUriDrag *d = new QUriDrag( this, 0 );
      if ( !d ) return 0;
      d->setFileNames( l );
      return (QDragObject*) d;
    }
};

class kmk_files_KListView : public KListView
{
Q_OBJECT
public:
    kmk_files_KListView (QWidget *parent = 0, const char *name = 0)
    : KListView ( parent, name )
    {}
    virtual ~kmk_files_KListView()
    {}
protected:
    virtual QDragObject *dragObject()
    {
      QStringList l;
      KListViewItem *node = (KListViewItem*) this->firstChild();
      if ( !node ) return 0;

      while ( node )
       {
         if ( node->isSelected() )      l << node->text( 0 ) + "/" + node->text( 1 );
         node = (KListViewItem*) node->nextSibling();
       }

      QUriDrag *d = new QUriDrag( this, 0 );
      if ( !d ) return 0;
      d->setFileNames( l );
      return (QDragObject*) d;
    }
};

/**
 * @short kmk is the main widget which actually makes KMK what it is
 *
 * This Widget extends the functionality of QListView to honor the system
 * wide settings for Single Click/Double Click mode, AutoSelection and
 * ChangeCursorOverLink (TM).
 *
 * There is a new signal executed(). It gets connected to either
 * QListView::clicked() or QListView::doubleClicked() depending on the KDE
 * wide Single Click/Double Click settings. It is strongly recommended that
 * you use this signal instead of the above mentioned. This way you don�t
 * need to care about the current settings.
 * If you want to get informed when the user selects something connect to the
 * QListView::selectionChanged() signal.
 *
 * Drag-and-Drop is supported with the signal dropped(), just setAcceptDrops(true)
 * and connect it to a suitable slot.
 * To see where you are dropping, setDropVisualizer(true).
 * And also you'll need acceptDrag(QDropEvent*)
 *
 * KListView is drag-enabled, too: to benefit from that you have to derive from it.
 * Reimplement dragObject() and (possibly) startDrag(),
 * and setDragEnabled(true).
 *
 * @author Plamen Petrov <carpo@abv.bg>
 * @version 0.11
 */
class kmk : public KMainWindow
{
  Q_OBJECT
public:
  /**
   * Default constructor
   */
  kmk();

  /**
   * Default Destructor
   */
  virtual ~kmk();

private slots:
  /*$PRIVATE_SLOTS$*/
  /**
   *	Handle searching trough the selected dir - scan for files
   *	we recognize, and add them accordingly to the catalog tree;
   *	This SLOT initially cleans all contents of MusicCatalog,
   *	the Catalog tree KListView, and the two File list QTables
   */
  void slotFileNew();
  void slotFileOpen();
  void slotCatalogAddNewFolder();
/** Saves the catalog in memory to the file @p catalogFileName;
 *  If the string, being a filename in @p catalogFileName does not have
 *  a .kmk extension - this slot adds it
 */
  void slotFileSave();
  void slotFileSaveAs();
  void slotCatalogFileClose();
  void slotCatalogFileStats();
  void slotFileQuit();

  void slotProgramSettings();

  /**
   * This is where the setup for kmkTagEdit takes place:
   * a QTable::selection(s) is traversersed and relevant data
   * extracted in MmDataList, for user manipulation in the TagEditor
   * modal() dialog
   */
  void slotTagEdit();
  void slotListTableToggleTitle();

  /**
   * A private slot for kmk which takes care of making the external player
   * play the previous song - either the song before the current in its
   * playlist, or if it is in SHUFFLE mode - the previous played
   */
  void slotPlayerPrevious();

  /**
   * A private slot for kmk which takes care of making the external player
   * play; if it was playing something at the time the command is issued
   * there should be no effect whatsoever
   */
  void slotPlayerPlay();

  /**
   * A private slot for kmk which makes the external player
   * pause - in reality - if it was paused, it will continue;
   * if it was playing - it will pause
   */
  void slotPlayerPause();

  /**
   * A private slot for kmk which makes the external player stop
   * playing if it is; if it isn't - there is no effect
   */
  void slotPlayerStop();

  /**
   * Private kmk slot, which essentially behaves like kmk::playerPrevious()
   * only difference is the "direction"
   */
  void slotPlayerNext();

  /**
   * This private kmk slot is resposible for enqueing whatever is selected
   * in the FileList() QTables, or SearchFileList() QTable - its native
   * call environment is from a popup menu after recieved
   * contextMenuRequested() signal
   */
  void slotPlayerEnqueue();

  /**
   * Uses kmk::playerDir to enqueue
   */
  void slotPlayerEnqueueDir();

  /**
   * Uses kmk::playerDir to enqueue selected folder AND all its subfolder(s) contents
   */
  void slotPlayerEnqueueDirSubdirs();

  /**
   * Uses kmk::playerDir to play
   */
  void slotPlayerPlayDir();

  /**
   * Uses kmk::playerDir to play selected folder AND all its subfolder(s) contents
   */
  void slotPlayerPlayDirSubdirs();

  /**
   * This private slot should play whatever selection in FileList()
   * or kmkWidget::SearchTreeList() QTable objects 
   */
  void slotPlayerPlaySelection();

  /**
   * Triggers update on currently selected folder in TreeListView
   */
  void slotCatalogUpdateSubtree();

  void slotTreeListViewPopupMenuRequested( QListViewItem* itm, const QPoint &pos, int col );
  void slotTreeListViewCurrentChanged( QListViewItem * itm );

  void slotFileListTablePopupMenuRequested( QListViewItem* itm, const QPoint &pos, int col );
  void slotSearchListTablePopupMenuRequested( QListViewItem* itm, const QPoint &pos, int col );

  /**
   * Available for selection in the search results table popup menu ONLY when
   * one item (row) is selected; shows the catalag AND selects the folder
   * containing the selected item (currently only files are listed). 
   */
  void slotLocateRequested();

  void slotSearchButtonClicked();
  void slotClearButtonClicked();
  void slotDoneButtonClicked();

  void slotUpdateProgressDisp();

private:

    QWidget * main_container_widget;
    QTabWidget* tabWidget1;
    QWidget* tab;
    QSplitter* splitter1;
    kmk_tree_KListView* treeListView;
    kmk_files_KListView* filesListView;
    kmk_files_KListView* searchFilesListView;
    QWidget* tab_2;
    QTabWidget* tabWidget2;
    QWidget* tab_3;
    QPushButton* pbSearch;
    QPushButton* pbClear;
    QPushButton* pbDone;
    QFrame* frame1;
    QLabel* textLabel1;
    KLineEdit* leSearchFor;
    QWidget* tab_4;

  KToolBar* fileToolsToolbar;
  KToolBar* playerActionsToolbar;
  KAction* fileNewAction;
  KAction* fileOpenAction;
  KAction* catalogAddNewFolderAction;
  KAction* fileSaveAction;
  KAction* fileSaveAsAction;
  KAction* fileCatalogFileClose;
  KAction* fileCatalogFileStats;
  KAction* fileQuitAction;
  KAction* programSettingsAction;
  KAction* listTableToggleTitleAction;
  KAction* TagEditAction;
  KAction* listTableLocateAction;
  KAction* playerPreviousAction;
  KAction* playerPlayAction;
  KAction* playerPauseAction;
  KAction* playerStopAction;
  KAction* playerNextAction;
  KAction* playerEnqueueAction;
  KAction* playerEnqueueDirAction;
  KAction* playerEnqueueDirSubdirsAction;
  KAction* playerPlayDirAction;
  KAction* playerPlayDirSubdirsAction;
  KAction* playerPlaySelectionAction;
  KAction* catalogUpdateSubtreeAction;

  QPopupMenu* CTree_PopupMenu;
  QPopupMenu* CList_PopupMenu;
  QPopupMenu* CSearchList_PopupMenu;

  void init_interface();

//  kmkWidget* kmk_widg;
  KListViewItem* cur_vlItem;
  KListViewItem* listViewRootItem;
  MmDataList MusicCatalog;

  enum CatalogStateEnum {
      NoCatalog=0,
      Modified,
      Saved
  };

  CatalogStateEnum CatalogState;
  void setCatalogStateAndUpdate( const kmk::CatalogStateEnum state );

  /**
   * locateMode [bool] if set to TRUE indicates to the
   * treeListViewCurrentChanged method that it should select
   * the row, containing DelSelFile AND DelSelDir, if it finds them
   * during the FileList() table update...
   */
  bool locateMode;
  QString DelSelFile;
  QString DelSelDir;

  void clearCatalogData( const bool UpdateState = TRUE );

  /**
   * Used to indicate whether we should stop a long disk operation:
   * scanning, updating, etc; checked were appropriate: 
   * in traverse_tree( const QString& dir ); and bytes_to_read_by_traverse( const QString& dir );
   */
  bool break_long_disk_operation;
  kmk_progress_disp* kmkProgress;
  QTimer* kmkProgressTimer;
  QTime* kmkSmooth;

  bool FileSaveCalledFromFileSaveAs;

  /** Finds all files in kmkWidget::TreeList() currentItem and all its subfolders
   *  and passes them to player_bin with parameter @param act
   */
  void playerDir( uint act );

  /**
   * This private function implements recursive search at the
   * top level dir, specified in the @p dir parameter;
   * currently there are absolutely no checks if the dir is valid -
   * first, because this function is called from controlled environment;
   * second, as I described - it is recursive, so we don't want to loose
   * any speed, checking if we are called with valid input
   */
  unsigned long traverse_tree( const QString& dir, const QString& pf = "AUTO" );

  /**
   * Remove a folder inside a MmData list and all of its children
   * (file AND folders)
   * NOTE: @p folder must NOT have the slash / symbol in the end!!!
   * @return 0        - on succes
   *         non-zero - error
   */
  uint rem_MmData_Folder( MmDataList* list, const QString& folder );

  /**
   * This private function starts by clearing current contents of
   * Tree KListView; next it goes trough kmk's MusicCatalog, recreating
   * the tree, described in the MmData structure, inside the Tree view
   */
  void update_tree_view();
  bool tree_needs_update;
  void bytes_to_read_by_traverse( const QString& dir );
  void generateListViewXML( const KListView *list, QDomDocument doc, QDomElement e );
  uint generateListViewSubtreeXML( const KListViewItem *item, QDomDocument doc, QDomElement e, const uint add_lv );
  MmData* findMmData( const QString& folder, const QString& filename );
  void loadCatalog( const QString & fileName );
  unsigned int subDlevel;
  /**
   * Holds TRUE if Enqueue dir in kmkWidget::FileList() popup
   * menu should add subdirs, FALSE if not
   */
  bool _kmk_include_subdirs;
  /**
   * This boolean variable, local and private for kmk indicates
   * wether a popup menu, including id3 tag editor submenu
   * was requested at file list in the catalog tab,
   * or at the search result file list.
   */
  bool _popup_at_search;
  const QString formatted_string_from_seconds( const Q_ULLONG t );
  const bool catalog_has_dir( const QString & );
  void update_catalog_stats( MmDataList* list );
  /** Some stats variables
   * \endcode */
  Q_ULLONG bytes_to_read;
  Q_ULLONG files_to_read;
  Q_ULLONG total_bytes;
  Q_ULLONG total_files;
  Q_ULLONG total_folders;
  Q_ULLONG total_play_time;
  Q_ULLONG average_file_size;
  QString savedCaption;
  QString catalogFileName;

  KmkGlobalSettings * s;
  KmkExtPlayer * player;

protected:
  void closeEvent( QCloseEvent* ce );

  QVBoxLayout* kmkwidgetbaseLayout;
  QVBoxLayout* tabLayout;
  QVBoxLayout* tabLayout_2;
  QVBoxLayout* layout5;

};

#endif // _KMK_H_
