/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qtooltip.h>
#include <qfileinfo.h>
#include <klocale.h>
#include <kdebug.h>
#include <kmessagebox.h>
#include <kapplication.h>

#include "tag.h"
#include "fileref.h"
#include "tstring.h"
#include "tfile.h"

#include "kmktaged.h"

#define TDLG "TagEdit Dialog: "

kmkTagEdit::kmkTagEdit( MmDataPList *files, uint *rm_flag )
  : kmkTagEdBase( 0, "KMK Tag Editor Dialog", FALSE, Qt::WType_Dialog |  Qt::WDestructiveClose )
{
  s = new KmkGlobalSettings;
  Q_CHECK_PTR( s );
  s->readSettings( kapp->config() );
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << TDLG"constructor: read config, now will use it..." << endl;

  setModal( TRUE );
  cbSaveOnNavigate->setChecked( s->saveOnNavigate() );
  /* Disable Save button, as we have no chages yet */
  pbSave->setEnabled( FALSE ); content_changed = FALSE; any_changed = FALSE;
  remote_flag = rm_flag; *remote_flag = 0;
  if (files)
   {
     fs = files;
     if(fs->isEmpty()) {
        KMessageBox::sorry( this, i18n("The Tag editor dialog cannot work without data."),
                                  i18n("KDE Music Kataloger") );
        done( Rejected );  // we have WDestructiveClose, so this should delete the dialog
        return;
      }
// Here we check if any of the items the given ptrlist points to is FOLDER. If so - bail out.
     for ( current = fs->first(); current; current = fs->next() )
      if((*current).IsDir())
       {
         if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
          kdDebug() << TDLG"constructor: DIR in recv. list! Stopping initialisation..." << endl;
         done( Rejected );
       }
     current = fs->first();
     pbFirst->setEnabled( FALSE ); pbPrev->setEnabled( FALSE );
     if(fs->count()==1)   { pbNext->setEnabled( FALSE );  pbLast->setEnabled( FALSE ); }

     set_on_display( current );

     if( s->dbg() & KMK_DBG_SIGNALS )
      kdDebug() << TDLG"constructor: connecting signals to slots... " << endl;

     connect( pbFirst, SIGNAL( clicked() ), this, SLOT( slotClicked_pbFirst() ) );
     connect( pbPrev,  SIGNAL( clicked() ), this, SLOT( slotClicked_pbPrev() ) );
     connect( pbNext,  SIGNAL( clicked() ), this, SLOT( slotClicked_pbNext() ) );
     connect( pbLast,  SIGNAL( clicked() ), this, SLOT( slotClicked_pbLast() ) );
     connect( pbSave,  SIGNAL( clicked() ), this, SLOT( slotClicked_pbSave() ) );
     connect( pbClose, SIGNAL( clicked() ), this, SLOT( slotClicked_pbClose() ) );

     handle_changes = FALSE;
     connect( leArtist,   SIGNAL( textChanged( const QString & ) ), this, SLOT( slotHandle_tag_change( const QString & ) ) );
     connect( leTitle,    SIGNAL( textChanged( const QString & ) ), this, SLOT( slotHandle_tag_change( const QString & ) ) );
     connect( leAlbum,    SIGNAL( textChanged( const QString & ) ), this, SLOT( slotHandle_tag_change( const QString & ) ) );
     connect( leYear,     SIGNAL( textChanged( const QString & ) ), this, SLOT( slotHandle_tag_change( const QString & ) ) );
     connect( leTrackNum, SIGNAL( textChanged( const QString & ) ), this, SLOT( slotHandle_tag_change( const QString & ) ) );
     connect( leGenre,    SIGNAL( textChanged( const QString & ) ), this, SLOT( slotHandle_tag_change( const QString & ) ) );
     connect( leComment,  SIGNAL( textChanged( const QString & ) ), this, SLOT( slotHandle_tag_change( const QString & ) ) );
     handle_changes = TRUE;

     if( s->dbg() & KMK_DBG_SIGNALS )
      kdDebug() << TDLG"constructor: done connecting signals!" << endl;

     exec();
   }
}

kmkTagEdit::~kmkTagEdit()
{
  if (fs)
  {
    if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
     kdDebug() << TDLG"destructor: removing TagEd temp data from list..." << endl;
    for ( current = fs->first(); current; current = fs->next() )
     (*current).setIsDir( FALSE );
    if( s->dbg() & KMK_DBG_CAT_MEM_OPS ) kdDebug() << TDLG"destructor: done." << endl;
  }
  if( s->dbg() & KMK_DBG_CONFIG )
               kdDebug() << TDLG"destructor: saving setting from SaveOnNavigate checkbox..." << endl;
  s->setSaveOnNavigate( cbSaveOnNavigate->isChecked() );
  if( s->dbg() & KMK_DBG_CONFIG )
               kdDebug() << TDLG"destructor: ...and writing it on disk..." << endl;
  s->saveSettings( kapp->config() );
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << TDLG"destructor: ...done!" << endl;
  delete s;
}

void kmkTagEdit::save_current()
{
  if( content_changed )
   {
    if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
       kdDebug() << TDLG"save_current: saving changed content in memory..." << endl;
    (*current).setArtist( leArtist->text() );
    (*current).setTitle( leTitle->text()  );
    (*current).setAlbum( leAlbum->text() );
    (*current).setTrackNum( leTrackNum->text().toInt() );
    (*current).setYear( leYear->text().toInt() );
    (*current).setGenre( leGenre->text() );
    (*current).setComment( leComment->text() );
    if( s->dbg() ) kdDebug() << TDLG"save_current: SHOULD BE saving tag in file..." << endl;
    if( (*current).IsReadOnly() )
      KMessageBox::sorry( this, i18n("According to the file info - it is read only.\n"
                                     "This should have stopped you from getting here -\n"
                                     "trying to save to a Read-only file."),
                                i18n("KDE Music Kataloger") );
    using namespace TagLib;
/* Use TagLib's functions to save meta data and audio properties;
   This tells TagLib to NOT read tags, and be as fast as possible */
    QString fn = (*current).Folder()+"/"+(*current).FileName();
    if( s->dbg() & KMK_DBG_FS_OPS )
       kdDebug() << TDLG"save_current: saving tag for file: " << fn << "..." << endl;
    TagLib::FileRef f( fn, FALSE, TagLib::AudioProperties::Fast );
/* If TagLib found the file to be valid - then add it to the catalog.
 Maybe here would be a good place to update some global catalog stats,
 like total storage used by the collection we are cataloguining, number
 of files in it, average bitrate, highest one, lowest one, biggest file,
 smallest file... and anything else someone might consider "interesting"!*/
    if( f.file()->isValid() )
     {
      TagLib::String u;
      u = (*current).Artist().latin1();   f.tag()->setArtist( u );
      u = (*current).Title().latin1();    f.tag()->setTitle( u );
      u = (*current).Album().latin1();    f.tag()->setAlbum( u );
      u = (*current).Genre().latin1();    f.tag()->setGenre( u );
      u = (*current).Comment().latin1();  f.tag()->setComment( u );
      f.tag()->setTrack( (*current).TrackNum() );
      f.tag()->setYear( (*current).Year() );
      if( !f.save() )
         KMessageBox::sorry( this, i18n("Something prevented KMK from writing\n"
                                        "the tag to file [%1].").arg(fn),
                                   i18n("KDE Music Kataloger") );
      else
       {  // tag is saved, now get the last modified time for it
         if( s->dbg() & KMK_DBG_FS_OPS )
          kdDebug() << TDLG"save_current: getting [" << fn << "]'s \"last modified\" time..." << endl;
         QFileInfo *fi = new QFileInfo( fn );
         if( fi )
          {
           if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
             kdDebug() << TDLG"save_current: fixing [" << fn << "]'s \"last modified\" time..." << endl;
           (*current).setModifiedTime( fi->lastModified().toTime_t() );
           if( s->dbg() & KMK_DBG_CAT_MEM_OPS )
             kdDebug() << TDLG"save_current: marking [" << fn << "] as \"SAVED\" (not modified)..." << endl;
           (*current).setIsDir( FALSE );
           delete fi;
          }
       }
     }
// after changing a file in the list, mark the hole list as changed,
// and also set the remote_flag accordingly - set on first save
    any_changed = TRUE;  *remote_flag = 1;
    if( s->dbg() & KMK_DBG_FS_OPS )
       kdDebug() << TDLG"save_current: ...done!" << endl;
   }
  content_changed = FALSE;
  pbSave->setEnabled( FALSE );
}

void kmkTagEdit::slotHandle_tag_change( const QString & m )
{
  Q_UNUSED(m);
  if(!handle_changes) return;
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotHandle_tag_change: processing request..." << endl;
  bool new_tag_is_different = FALSE;
  if( leArtist->text().compare( (*current).Artist() )!=0 )                    new_tag_is_different = TRUE;
  if( leTitle->text().compare( (*current).Title()  )!=0 )                     new_tag_is_different = TRUE;
  if( leAlbum->text().compare( (*current).Album() )!=0 )                      new_tag_is_different = TRUE;
  if( leTrackNum->text().compare
   (((*current).TrackNum())?QString::number((*current).TrackNum() ):"")!=0 )  new_tag_is_different = TRUE;
  if( leYear->text().compare
   (((*current).Year())?QString::number( (*current).Year() ):"")!=0 )         new_tag_is_different = TRUE;
  if( leGenre->text().compare( (*current).Genre() )!=0 )                      new_tag_is_different = TRUE;
  if( leComment->text().compare( (*current).Comment() )!=0 )                  new_tag_is_different = TRUE;
  if( new_tag_is_different )
   {
    content_changed = TRUE;
    pbSave->setEnabled( TRUE );
//  we use MmData's IsDir property to know whether a file's tag is changed, and should be saved
    (*current).setIsDir( TRUE );
   }
  else // maybe the user just set the data back to where it was...
   {
    content_changed = FALSE;
    pbSave->setEnabled( FALSE );
    (*current).setIsDir( FALSE );
   }
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotHandle_tag_change: done!" << endl;
}

void kmkTagEdit::set_on_display( MmData *i )
{
  if( s->dbg() & KMK_DBG_OTHER ) kdDebug() << TDLG"set_on_display:  " <<
      (( (*i).IsDir() )?"item appears modified - enabling Save button"
                       :"no save needed - disabling Save button") << endl;
  if( (*i).IsDir() ) pbSave->setEnabled( TRUE );
  else  pbSave->setEnabled( FALSE );
  handle_changes = FALSE;
  QToolTip::remove( tl_fn );
  QToolTip::add( tl_fn, (*i).Folder() + "/" + (*i).FileName() );
  tl_fn->setText( (*i).Folder() + "/" + (*i).FileName() );
  leArtist->setText( (*i).Artist() );
  leTitle->setText( (*i).Title() );
  leAlbum->setText( (*i).Album() );
  leTrackNum->setText( ((*i).TrackNum()!=0)?QString::number( (*i).TrackNum() ):"" );
  leYear->setText( ((*i).Year()!=0)?QString::number( (*i).Year() ):"" );
  leGenre->setText( (*i).Genre() );
  leComment->setText( (*i).Comment() );
  if( (*i).IsReadOnly() )
   {
     leArtist->setEnabled( FALSE );
     leTitle->setEnabled( FALSE );
     leAlbum->setEnabled( FALSE );
     leTrackNum->setEnabled( FALSE );
     leYear->setEnabled( FALSE );
     leGenre->setEnabled( FALSE );
     leComment->setEnabled( FALSE );
   }
  else
   {
     leArtist->setEnabled( TRUE );
     leTitle->setEnabled( TRUE );
     leAlbum->setEnabled( TRUE );
     leTrackNum->setEnabled( TRUE );
     leYear->setEnabled( TRUE );
     leGenre->setEnabled( TRUE );
     leComment->setEnabled( TRUE );
   }
  handle_changes = TRUE;
}

void kmkTagEdit::slotClicked_pbFirst()
{
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbFirst: processing request..." << endl;
  if( cbSaveOnNavigate->isChecked() ) save_current();
  if(current != fs->getFirst())
   {
     current = fs->first();
     if( fs->count()>1 )
      {
        pbNext->setEnabled( TRUE );
        pbLast->setEnabled( TRUE );
      }
     set_on_display( current );
     pbPrev->setEnabled( FALSE );
     pbFirst->setEnabled( FALSE );
   }
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbFirst: done!" << endl;
}

void kmkTagEdit::slotClicked_pbPrev()
{
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbPrev: processing request..." << endl;
//  pbSave->setEnabled( FALSE );
  if( cbSaveOnNavigate->isChecked() ) save_current();
  if(current != fs->getFirst())
   {
     current = fs->prev();
     if( fs->count()>1 )
      {
        pbNext->setEnabled( TRUE );
        pbLast->setEnabled( TRUE );
      }
     set_on_display( current );
     if( (current == fs->getFirst()) || (fs->count() == 2) )
      {
        pbPrev->setEnabled( FALSE );
        pbFirst->setEnabled( FALSE );
      }
   }
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbPrev: done!" << endl;
}

void kmkTagEdit::slotClicked_pbNext()
{
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbNext: processing request..." << endl;
  if( cbSaveOnNavigate->isChecked() ) save_current();
  if( current != fs->getLast() )
   {
     current = fs->next();
     if( fs->count()>1 )
      {
        pbPrev->setEnabled( TRUE );
        pbFirst->setEnabled( TRUE );
      }
     set_on_display( current );
     if( (current == fs->getLast()) || (fs->count() == 2) )
      {
        pbNext->setEnabled( FALSE );
        pbLast->setEnabled( FALSE );
      }
   }
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbNext: done!" << endl;
}

void kmkTagEdit::slotClicked_pbLast()
{
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbLast: processing request..." << endl;
  if( cbSaveOnNavigate->isChecked() ) save_current();
  if( current != fs->getLast())
   {
     current = fs->last();
     if( fs->count()>1 )
      {
        pbPrev->setEnabled( TRUE );
        pbFirst->setEnabled( TRUE );
      }
     set_on_display( current );
     pbNext->setEnabled( FALSE );
     pbLast->setEnabled( FALSE );
   }
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbLast: done!" << endl;
}

void kmkTagEdit::slotClicked_pbSave()
{
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbSave: processing request..." << endl;
  save_current();
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbSave: done!" << endl;
}

void kmkTagEdit::slotClicked_pbClose()
{
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbClose: processing request..." << endl;
  if( content_changed )
    switch( KMessageBox::questionYesNoCancel( this,
           i18n("The current file's tags were modified. Do you want "
                "to save the changes before closing the dialog?"),
           i18n("KDE Music Kataloger") ) )   {
      case KMessageBox::Yes: // Save & Exit
          save_current();
          close();
          break;
      case KMessageBox::No: // Discard - just Exit
          close();
          break;
      case KMessageBox::Cancel: // Cancel - no nothing
          break;
     }
  else close();
  if( s->dbg() & KMK_DBG_SIGNALS )
   kdDebug() << TDLG"slotClicked_pbClose: done!" << endl;
}

#include "kmktaged.moc"
