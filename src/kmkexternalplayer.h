/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KMKEXTERNALPLAYER_H
#define KMKEXTERNALPLAYER_H

#include <qobject.h>
#include <qwidget.h>

#include "kmkglobalsettings.h"

#define KMK_NUMBER_OF_SUPPORTED_PLAYERS		3
/**
 * @short supported external player types
 *
 * The first one should be equal to 0, so there is mapping between player types
 * if they are listed in the same order for KmkSettingsDialog::cmb_Player too
 */
enum KmkExtPlayers { xmms=0, amarok=1, audacious=2, mplayer=3, xmms2=4 };
static const char * kmkExtPlayersNames[] =
{
  "X Multimedia System",
  "Amarok!",
  "Audacious",
  "MPlayer",
  "xmms2",
};

/**
 * @short Class to manage calling external players
 *
 * The KmkExtPlayer class reads from KmkGlobalSettings the selected player,
 * and processes request to pass to it playlist files (.M3U format).
 * This class exists for future expandability, and also for cetranlized
 * external player management, as some players request special "treatment"....
 *
 * KmkExtPlayer should handle troubles internally, or in the worst case -
 * signal to the app to ask the user for settings/different player/etc.
 *
 * @author Plamen Petrov <carpo@abv.bg>, <pvp@tk.ru.acad.bg>
 * @version 0.1
 */
class KmkExtPlayer : public QObject
{
    Q_OBJECT

public:
  /**
   * @short Default constructor
   *
   * This is the default constructor
   */
    KmkExtPlayer( QWidget* parent = 0, const char* name = 0 );
  /**
   * @short Default destructor
   *
   * This is the default destructor
   */
    ~KmkExtPlayer();

  /** This method is equivalent to hitting the "PLAY" button
   *  on the external player, if it supports it.
   */
    void play();

  /** This method is equivalent to hitting the "PAUSE" button
   *  on the external player, if it supports it.
   */
    void pause();

  /** This method is equivalent to hitting the "STOP" button
   *  on the external player, if it supports it.
   */
    void stop();

  /** This method is equivalent to hitting the "NEXT/FORWARD" button
   *  on the external player, if it supports it.
   */
    void next();

  /** This method is equivalent to hitting the "PREVIOUS/BACKWARDS" button
   *  on the external player, if it supports it.
   */
    void previous();

  /** This method take care of passing the @param playlist_file file
   *  to the selected external player and sending it a play command;
   *  \code playPlaylist \endcode tries its best to start playing the
   *  playlist from its first song.
   */
    void playPlaylist( const QString& playlist_file );

  /** This method take care of passing the @param playlist_file file
   *  to the selected external player with request @param playlist_file
   *  to be \b ENQUEUE\b-ed, adding to the current one
   */
    void addPlaylist( const QString& playlist_file );


  /** Whenever the config settings which drive KmkExtPlayer get changed,
   *  this method should be called so that the new settings are used
   */
    void handleConfigChange();

private slots:
    void call_play();

private:
  /** This is the default destructor */
    unsigned short currentPlayerType;

  /** Some QStrings maybe used to store extra params,
    * passed to the player, specified by the user.
    */
    QString e_param1, e_param2, e_param3;

  /** This one is used to see 
    */
    KmkGlobalSettings * s;
};


#endif // KMKEXTERNALPLAYER_H
