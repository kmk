/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <kapplication.h>
#include <kdebug.h>
#include <qcombobox.h>
#include <qcheckbox.h>
#include <qpushbutton.h>

#include "kmksettings_dialog.h"
#include "kmkexternalplayer.h"
#define SDLG "Settings Dialog: "

//TODO: make the dialog remember initial settings and activate "Save and Apply" only when
// there really are changes to save

kmkSettingsDialog::kmkSettingsDialog( uint *rm_flag )
    :kmkSettingsBase( 0, "KMK Settings Dialog", FALSE, Qt::WType_Dialog |  Qt::WDestructiveClose )
{
  Q_UNUSED( rm_flag );

  s = new KmkGlobalSettings;
  Q_CHECK_PTR( s );
  s->readSettings( kapp->config() );

  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << SDLG"constructor: read config, now will use it..." << endl;
  cb_last->setChecked( s->loadLast() );
  cb_geometry->setChecked( s->saveGeo() );
  cb_autoAdj->setChecked( s->autoColumnWidth() );
  cmb_Player->clear();
  for( int er=0; er < KMK_NUMBER_OF_SUPPORTED_PLAYERS; er++)
   cmb_Player->insertItem( QString( kmkExtPlayersNames[er] ) );
  cmb_Player->setCurrentItem( s->extPlayer() );
  cmb_Player->setEnabled( TRUE );
  if( s->dbg() ) kdDebug() << SDLG"constructor: currently " << cmb_Player->count() <<
      " items in cmb_Player comboBox..." << endl;

  if( s->dbg() & KMK_DBG_SIGNALS )
      kdDebug() << SDLG"constructor: connecting signals to slots... " << endl;

  connect( pbSave,   SIGNAL( clicked() ), this, SLOT( slotClicked_pbSave()   ) );
  connect( pbCancel, SIGNAL( clicked() ), this, SLOT( slotClicked_pbCancel() ) );

  if( s->dbg() & KMK_DBG_SIGNALS )
      kdDebug() << SDLG"constructor: done connecting signals!" << endl;

  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << SDLG"constructor: done." << endl;
  setModal( TRUE );
  exec();
}

kmkSettingsDialog::~kmkSettingsDialog()
{
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << SDLG"destructor: deleting config object." << endl;
  delete s;
}

void kmkSettingsDialog::slotClicked_pbSave()
{
  if( s->dbg() & KMK_DBG_SIGNALS ) kdDebug() << SDLG"slotClicked_pbSave: processing request..." << endl;
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << SDLG"slotClicked_pbSave: saving config..." << endl;
  s->setLoadLast( cb_last->isChecked() );
  s->setSaveGeo( cb_geometry->isChecked() );
  s->setAutoColumnWidth( cb_autoAdj->isChecked() );
  s->setExtPlayer( cmb_Player->currentItem() );
  s->saveSettings( kapp->config() );

  if( s->dbg() ) kdDebug() << SDLG"slotClicked_pbSave: cmb_Player current item: "
    << cmb_Player->currentItem() << endl;
  done( Accepted );  // we have WDestructiveClose, so this should delete the dialog
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << SDLG"slotClicked_pbSave: done." << endl;
  if( s->dbg() & KMK_DBG_SIGNALS ) kdDebug() << SDLG"slotClicked_pbSave: done. " << endl;
}

void kmkSettingsDialog::slotClicked_pbCancel()
{
  if( s->dbg() & KMK_DBG_SIGNALS ) kdDebug() << SDLG"slotClicked_pbCancel: processing request..." << endl;
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << SDLG"slotClicked_pbCancel: NOT saving config..." << endl;
  done( Rejected );  // we have WDestructiveClose, so this should delete the dialog
  if( s->dbg() & KMK_DBG_SIGNALS ) kdDebug() << SDLG"slotClicked_pbCancel: done. " << endl;
}

void kmkSettingsDialog::slotHandle_tag_change( const QString & m )
{
  Q_UNUSED( m );
}

#include "kmksettings_dialog.moc"
