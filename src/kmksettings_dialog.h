/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef KMKSETTINGS_DIALOG_H
#define KMKSETTINGS_DIALOG_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "kmkglobalsettings.h"
#include "kmksettingsbase.h"

/**
 * @short A settings dialog, specific to KMK.
 *
 * @author Plamen Petrov <carpo@abv.bg>, <pvp@tk.ru.acad.bg>
 * @version 0.3
 */
class kmkSettingsDialog: public kmkSettingsBase {
  Q_OBJECT
public:
  /**
   * @param files   - holds a pointer to initially filled MmDataList, containing
   *       files to be processed by kmkTagEdit
   * @param SoN     - gets passed on to the SaveOnNavigate checkbox setChecked() method
   * @param rm_flag - a pointer to a int, used to indicate whether the hole list
   *    has saved changes ( 1 ), or hasn't been touched ( 0 )
   * Use code like this:
   * \code
   *    int * tags_changed = new int;
   *    new kmkTagEdit( tag_list, SaveOnNavigate, tags_changed );
   *    if( *tags_changed > 0)
   *       kdDebug() << "TagEditDialog changed"
   *                    " some file(s). " << endl;
   *    else
   *      kdDebug() << "No changes written to files"
   *                   " by TagEditDialog. " << endl;
   * \endcode
   */
  kmkSettingsDialog( uint *rm_flag );

  /**
   * @short Default Destructor
   *
   * If the MmDataPList passed at the constructor is valid - then
   * all items' property MmIsDir is set to FALSE, because these items
   * are files, and if they aren't (they are folders then) - then they
   * shouldn't have gotten in it at all, and there is a bug...
   *
   * Because kmkTagEdit uses MmData::MmIsDir to remember if a file was
   * modified, the destructor sets that property of all items in
   * the list it got handed in the constructor to FALSE
   *
   * @see MmData
   */
  virtual ~kmkSettingsDialog();

private slots:
  /**
   * @short Some informative description
   *
   * This method does something that needs to be documented
   */
  void slotClicked_pbSave();
  /**
   * @short Some informative description
   *
   * This method does something that needs to be documented
   *
   * @since 0.2
   */
  void slotClicked_pbCancel();
  /**
   * @short Private method to handle tags changing
   *
   * If the user changes anything in the dialog, this method
   * is called to mark the MmData item displayed as CHANGED,
   * i.e. the Save button in the dialog should be enabled;
   * As the TagEditor only works with files only, the
   * MmData::MmIsDir property is used to store this info.
   *
   * @see MmData
   */
  void slotHandle_tag_change( const QString & );

private:
  /**
   * @p any_changed is used as a return value of the exec() slot;
   * If any of the files in the list passed to the constructor got
   * changed - this property, used as a flag, indicates that
   */
  bool any_changed;
  /**
   * @p content_changed is used internally to monitor for changes
   * in any of the tags, to enable proper Save button activation
   */
  bool content_changed;
  /**
   * @p handle_changes is a flag for the Tag edit dialog itself -
   * it marks whether textChanged signal should be processed or
   * discarded, as is the case when the dialog itself changes
   * the text in the appropriate fields, for navigation
   */
  bool handle_changes;
  /**
   * @short Some informative description
   *
   * Something that needs to be documented
   */
  uint * remote_flag;
  /**
   * @short The KmkGlobalSettings object used to get config data
   *
   * The pointed object is used to get settings data, e.g. debug
   *
   * @since 0.24
   */
  KmkGlobalSettings * s;
};

#endif // KMKSETTINGS_DIALOG_H
