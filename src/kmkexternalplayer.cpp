/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "kmkexternalplayer.h"

#include <kapplication.h>
#include <kdebug.h>
#include <qtimer.h>
#include <qprocess.h>
#include "kmkglobalsettings.h"

#define EP "KmkExtPlayer: "

KmkExtPlayer::KmkExtPlayer( QWidget* parent, const char* name )
 :QObject( parent, name )
{
  s = new KmkGlobalSettings;
  Q_CHECK_PTR( s );

  s->readSettings( kapp->config() );
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << EP"constructor: read config, now will use it..." << endl;
  handleConfigChange();

  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << EP"constructor: done." << endl;

}

KmkExtPlayer::~KmkExtPlayer()
{
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << EP"destructor: deleting config object..." << endl;
  delete s;
}

  /** This method is equivalent to hitting the "PLAY" button
   *  on the external player, if it supports it.
   */
void KmkExtPlayer::play()
{
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"play: processing request..." << endl;
  QProcess* ext_prog = new QProcess( this, "qproc_by_play");
  ext_prog->clearArguments();
  switch(currentPlayerType){
   case xmms :
      ext_prog->addArgument( "xmms" );
      ext_prog->addArgument( "--play" );
      break;
   case amarok :
      ext_prog->addArgument( "amarok" );
      ext_prog->addArgument( "--play" );
      break;
   case audacious :
      ext_prog->addArgument( "audacious" );
      ext_prog->addArgument( "--play" );
      break;
/*   case mplayer :
      ext_prog->addArgument( "mplayer" );
      ext_prog->addArgument( "-p" );
      break;
*/
   default :
      ext_prog->addArgument( "true" );
      break;
  }
  if( s->dbg() & KMK_DBG_EXT_PLAYER )
   {
    kdDebug() << EP"play: will start the following:" << endl;
    QStringList list = ext_prog->arguments();
    QStringList::Iterator it = list.begin();
    while( it != list.end() ) {
      kdDebug() << EP"play:      " << *it << endl;
      ++it;
     }
   }
  ext_prog->launch( "" );
  delete ext_prog;
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"play: done!" << endl;
}

  /** This method is equivalent to hitting the "PAUSE" button
   *  on the external player, if it supports it.
   */
void KmkExtPlayer::pause()
{
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"pause: processing request..." << endl;
  QProcess* ext_prog = new QProcess( this, "qproc_by_pause");
  ext_prog->clearArguments();
  switch(currentPlayerType){
   case xmms :
      ext_prog->addArgument( "xmms" );
      ext_prog->addArgument( "--play-pause" );
      break;
   case amarok :
      ext_prog->addArgument( "amarok" );
      ext_prog->addArgument( "--play-pause" );
      break;
   case audacious :
      ext_prog->addArgument( "audacious" );
      ext_prog->addArgument( "--play-pause" );
      break;
/*   case mplayer :
      ext_prog->addArgument( "mplayer" );
      ext_prog->addArgument( "-p" );
      break;
*/
   default :
      ext_prog->addArgument( "true" );
      break;
  }
  if( s->dbg() & KMK_DBG_EXT_PLAYER )
   {
    kdDebug() << EP"pause: will start the following:" << endl;
    QStringList list = ext_prog->arguments();
    QStringList::Iterator it = list.begin();
    while( it != list.end() ) {
      kdDebug() << EP"pause:      " << *it << endl;
      ++it;
     }
   }
  ext_prog->launch( "" );
  delete ext_prog;
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"pause: done!" << endl;
}

  /** This method is equivalent to hitting the "STOP" button
   *  on the external player, if it supports it.
   */
void KmkExtPlayer::stop()
{
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"stop: processing request..." << endl;
  QProcess* ext_prog = new QProcess( this, "qproc_by_stop");
  ext_prog->clearArguments();
  switch(currentPlayerType){
   case xmms :
      ext_prog->addArgument( "xmms" );
      ext_prog->addArgument( "--stop" );
      break;
   case amarok :
      ext_prog->addArgument( "amarok" );
      ext_prog->addArgument( "--stop" );
      break;
   case audacious :
      ext_prog->addArgument( "audacious" );
      ext_prog->addArgument( "--stop" );
      break;
/*   case mplayer :
      ext_prog->addArgument( "mplayer" );
      ext_prog->addArgument( "-p" );
      break;
*/
   default :
      ext_prog->addArgument( "true" );
      break;
  }
  if( s->dbg() & KMK_DBG_EXT_PLAYER )
   {
    kdDebug() << EP"stop: will start the following:" << endl;
    QStringList list = ext_prog->arguments();
    QStringList::Iterator it = list.begin();
    while( it != list.end() ) {
      kdDebug() << EP"stop:      " << *it << endl;
      ++it;
     }
   }
  ext_prog->launch( "" );
  delete ext_prog;
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"stop: done!" << endl;
}

  /** This method is equivalent to hitting the "NEXT/FORWARD" button
   *  on the external player, if it supports it.
   */
void KmkExtPlayer::next()
{
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"next: processing request..." << endl;
  QProcess* ext_prog = new QProcess( this, "qproc_by_next");
  ext_prog->clearArguments();
  switch(currentPlayerType){
   case xmms :
      ext_prog->addArgument( "xmms" );
      ext_prog->addArgument( "--fwd" );
      break;
   case amarok :
      ext_prog->addArgument( "amarok" );
      ext_prog->addArgument( "--next" );
      break;
   case audacious :
      ext_prog->addArgument( "audacious" );
      ext_prog->addArgument( "--fwd" );
      break;
/*   case mplayer :
      ext_prog->addArgument( "mplayer" );
      ext_prog->addArgument( "-p" );
      break;
*/
   default :
      ext_prog->addArgument( "true" );
      break;
  }
  if( s->dbg() & KMK_DBG_EXT_PLAYER )
   {
    kdDebug() << EP"next: will start the following:" << endl;
    QStringList list = ext_prog->arguments();
    QStringList::Iterator it = list.begin();
    while( it != list.end() ) {
      kdDebug() << EP"next:      " << *it << endl;
      ++it;
     }
   }
  ext_prog->launch( "" );
  delete ext_prog;
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"next: done!" << endl;
}

  /** This method is equivalent to hitting the "PREVIOUS/BACKWARDS" button
   *  on the external player, if it supports it.
   */
void KmkExtPlayer::previous()
{
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"previous: processing request..." << endl;
  QProcess* ext_prog = new QProcess( this, "qproc_by_previous");
  ext_prog->clearArguments();
  switch(currentPlayerType){
   case xmms :
      ext_prog->addArgument( "xmms" );
      ext_prog->addArgument( "--rew" );
      break;
   case amarok :
      ext_prog->addArgument( "amarok" );
      ext_prog->addArgument( "--previous" );
      break;
   case audacious :
      ext_prog->addArgument( "audacious" );
      ext_prog->addArgument( "--rew" );
      break;
/*   case mplayer :
      ext_prog->addArgument( "mplayer" );
      ext_prog->addArgument( "-p" );
      break;
*/
   default :
      ext_prog->addArgument( "true" );
      break;
  }
  if( s->dbg() & KMK_DBG_EXT_PLAYER )
   {
    kdDebug() << EP"previous: will start the following:" << endl;
    QStringList list = ext_prog->arguments();
    QStringList::Iterator it = list.begin();
    while( it != list.end() ) {
      kdDebug() << EP"previous:      " << *it << endl;
      ++it;
     }
   }
  ext_prog->launch( "" );
  delete ext_prog;
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"previous: done!" << endl;
}

  /** This method take care of passing the @param playlist_file file
   *  to the selected external player and sending it a play command;
   *  \code playPlaylist \endcode tries its best to start playing the
   *  playlist from its first song.
   */
void KmkExtPlayer::playPlaylist( const QString& playlist_file )
{
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"playPlaylist: processing request..." << endl;
  QProcess* ext_prog = new QProcess( this, "qproc_by_playPlaylist");
  ext_prog->clearArguments();
  switch(currentPlayerType){
   case xmms :
      ext_prog->addArgument( "xmms" );
      ext_prog->addArgument( "--play" );
      ext_prog->addArgument( playlist_file );
      break;
   case amarok :
      ext_prog->addArgument( "amarok" );
      ext_prog->addArgument( "--load" );
      ext_prog->addArgument( playlist_file );
      break;
   case audacious :
      ext_prog->addArgument( "audacious" );
      ext_prog->addArgument( "--play" );
      ext_prog->addArgument( playlist_file );
      break;
/*   case mplayer :
      ext_prog->addArgument( "mplayer" );
      ext_prog->addArgument( "-p" );
      break;
*/
   default :
      ext_prog->addArgument( "true" );
      break;
  }
  if( s->dbg() & KMK_DBG_EXT_PLAYER )
   {
    kdDebug() << EP"playPlaylist: will start the following:" << endl;
    QStringList list = ext_prog->arguments();
    QStringList::Iterator it = list.begin();
    while( it != list.end() ) {
      kdDebug() << EP"playPlaylist:      " << *it << endl;
      ++it;
     }
   }
  ext_prog->launch( "" );
  if (currentPlayerType == amarok) //amarok need special treatment...
   {
    kdDebug() << EP"playPlaylist: setting up AMARAOK workaround/time trickery..." << endl;
    QTimer::singleShot( 2000, this, SLOT(call_play()) );
    kdDebug() << EP"playPlaylist: AMAROK shit (call it with play command after 2000ms) is done!" << endl;
  }
  delete ext_prog;
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"playPlaylist: done!" << endl;
}

  /** This method take care of passing the @param playlist_file file
   *  to the selected external player with request @param playlist_file
   *  to be \b ENQUEUE\b-ed, adding to the current one
   */
void KmkExtPlayer::addPlaylist( const QString& playlist_file )
{
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"addPlaylist: processing request..." << endl;
  QProcess* ext_prog = new QProcess( this, "qproc_by_addPlaylist");
  ext_prog->clearArguments();
  switch(currentPlayerType){
   case xmms :
      ext_prog->addArgument( "xmms" );
      ext_prog->addArgument( "--enqueue" );
      ext_prog->addArgument( playlist_file );
      break;
   case amarok :
      ext_prog->addArgument( "amarok" );
      ext_prog->addArgument( "--enqueue" );
      ext_prog->addArgument( playlist_file );
      break;
   case audacious :
      ext_prog->addArgument( "audacious" );
      ext_prog->addArgument( "--enqueue" );
      ext_prog->addArgument( playlist_file );
      break;
/*   case mplayer :
      ext_prog->addArgument( "mplayer" );
      ext_prog->addArgument( "-p" );
      break;
*/
   default :
      ext_prog->addArgument( "true" );
      break;
  }
  if( s->dbg() & KMK_DBG_EXT_PLAYER )
   {
    kdDebug() << EP"addPlaylist: will start the following:" << endl;
    QStringList list = ext_prog->arguments();
    QStringList::Iterator it = list.begin();
    while( it != list.end() ) {
      kdDebug() << EP"addPlaylist:      " << *it << endl;
      ++it;
     }
   }
  ext_prog->launch( "" );
  delete ext_prog;
  if( s->dbg() & KMK_DBG_EXT_PLAYER ) kdDebug() << EP"addPlaylist: done!" << endl;
}

  /** Whenever the config settings which drive KmkExtPlayer get changed,
   *  this method should be called so that the new settings are used
   */
void KmkExtPlayer::handleConfigChange()
{
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << EP"handleConfigChange: re-reading config..." << endl;
  s->readSettings( kapp->config() );
  currentPlayerType = s->extPlayer();
  if( currentPlayerType >= KMK_NUMBER_OF_SUPPORTED_PLAYERS )
   {
     if( s->dbg() & KMK_DBG_EXT_PLAYER )
      kdDebug() << EP"handleConfigChange: found invalid player type in config - fixing it..."<< endl;
    currentPlayerType = 0; // 0 should be "xmms"
   }
  if( s->dbg() & KMK_DBG_EXT_PLAYER )
      kdDebug() << EP"handleConfigChange: using [" << kmkExtPlayersNames[s->extPlayer()]
                << "] as external player."<< endl;
  if( s->dbg() & KMK_DBG_CONFIG ) kdDebug() << EP"handleConfigChange: done!" << endl;
}


void KmkExtPlayer::call_play()
{
  play();
}
