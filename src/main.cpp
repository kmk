/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <kapplication.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>
//#include <kfiledialog.h>
#include <kdebug.h>
#include "kmkglobalsettings.h"
#include "kmk.h"

static const char description[] = I18N_NOOP("Audio file manager for KDE");

static const char version[] = VERSION;

/*
KMK_DBG_CONFIG		1
 *   KMK_DBG_CATALOG_IO		2
 *   KMK_DBG_CAT_MEM_OPS	4
 *   KMK_DBG_EXT_PLAYER		8
 *   KMK_DBG_FS_OPS		16
 *   KMK_DBG_SIGNALS		32
 *   KMK_DBG_SETTINGS		64
 *
 *   KMK_DBG_ALL		255
*/

static KCmdLineOptions options[] =
{
     { "d", 0, 0 },
     { "debug <level>", I18N_NOOP("debug level bitmask, where <level> is\n"
                                 "the result of AND-ing the bits for the desired\n"
                                 "subsystem of KMK :\n"
                                 "\tbit 0 - config;\n"
                                 "\tbit 1 - catalog IO;\n"
                                 "\tbit 2 - catalog memory operations;\n"
                                 "\tbit 3 - external player;\n"
                                 "\tbit 4 - file system interaction;\n"
                                 "\tbit 5 - internal signal handling;\n"
                                 "\tbit 6 - applications settings interaction;\n"
                                 "\tbit 7 - everything else, e.g. none of the above\n"
                                 "So, to see all debug possible,\npass 255 (all bits set)"), "0" },
     { "+[URL(s)]", I18N_NOOP("file(s) to open"), 0 },
     { "lang <language>", I18N_NOOP("Set the GUI language"), 0 },
     KCmdLineLastOption
};

int main(int argc, char **argv)
{
    KAboutData aboutData( "kmk", "KMK", version, description, KAboutData::License_GPL,
                          "(c) 2007,2008 Plamen Petrov", 0, "http://carpo.iax.be/projects/kmk/" );
    aboutData.addAuthor( "Plamen Petrov", 0, "carpo@abv.bg", "http://carpo.iax.be/" );
    aboutData.addCredit( "Sebastian Trüg",
          I18N_NOOP("For writing the excellent app K3B, from which code was taken."), "trueg@k3b.org" );
    aboutData.addCredit( "Kate Draven AKA BorgQueen",
          I18N_NOOP("For spreading the word, testing and giving some really nice suggestions."),
                    "borgqueen@arklinux.net" );

    KCmdLineArgs::init(argc, argv, &aboutData);
    KCmdLineArgs::addCmdLineOptions( options );
    KApplication app;

    kmk *mainWin = 0;

/*    if (app.isRestored())
    {
// FIXME: The following line should be commented out for releases!
        kdDebug() << "main.cpp: Restoring kmk's previous session... " << endl;
//        RESTORE(kmk);
    }
    else
    {*/
//        KGlobal::locale()->insertCatalogue( "kmk" );
        // no session.. just start up normally
        KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
        if( args->isSet("lang") )
         {
          if( !KGlobal::locale()->setLanguage(args->getOption("lang")) )
            kdDebug() << "main.cpp: Unable to set to language " << args->getOption("lang")
                      << " current is: " << KGlobal::locale()->language() << endl;
         }

// get the value of the "debug" cmd line argument
        QCString debugOptionArg = args->getOption("debug");
// kmk will pass the debug level around as config setting:
// so, here we get ourselves a config object...
        KmkGlobalSettings * s = new KmkGlobalSettings;
        Q_CHECK_PTR( s );

// ...load the config...
        s->readSettings( kapp->config() );
        bool t = FALSE;

// ...set the debug level to the desired value...
// NOTE: kmk's current code (v0.23.15.4) doesn't respect this
// TODO: fix the above for v0.24
        s->setDbg( debugOptionArg.toUShort( &t ) );
        if( !t ) s->setDbg( 0 );

// ...and write it in the config file, so the rest of kmk knows better!
        s->saveSettings( kapp->config() );

        if( s->dbg() )
            kdDebug() << "main.cpp: Debug level set to " << s->dbg() << endl;

// If we were started with debug level other than 0, start spitting info
// RIGHT NOW!!!!
        if( s->dbg() )
         {
          kdDebug() << "main.cpp: kmk " << version <<" with RUNTIME DEBUG starting." << endl;
          kdDebug() << "main.cpp: got " << argc << " parameters. Here they are:" << endl;
          for ( int i = 0; i < argc; i++ )  kdDebug() << "main.cpp: param[" << i << "] : " << argv[i] << endl;
         }
// Get rid of the config object: we are done with it!
        delete s;

        mainWin = new kmk();
        app.setMainWidget( mainWin );
        mainWin->show();

        args->clear();
/*    }*/

    return app.exec();
}
