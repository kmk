/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _KMK_MMDATA_H_
#define _KMK_MMDATA_H_

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <qstring.h>
#include <qvaluelist.h>
#include <qptrlist.h>

/**
 * @short Class to store info for an audio file
 *
 * The MmData class represents all relevant data for a file we are
 * interested in in hopefully small footprint; it uses integers
 * and QStrings; All members for accessing the data are inline;
 * MmData provides 2 constructors: one without any parameters,
 * and one to fill in all fields at object creation time
 *
 * @author Plamen Petrov <carpo@abv.bg>
 * @version 0.2.0.3
 */
class MmData
    {
    public:

  /**
   * @short Default constructor with default values
   *
   * This constructor creates a MmData object and nullifies its fields
   */
        MmData(): MmFolder(""), MmFileName(""), MmArtist(""), MmTitle(""),
                  MmAlbum(""), MmGenre(""), MmComment(""), MmYear(0), MmTrackNum(0),
                  MmFileSize(0), MmModifiedTime(0), MmIsReadOnly(TRUE), MmIsDir(FALSE),
                  MmLength(0), MmChannels(0), MmSampleRate(0), MmBitRate(0)
        {}

  /**
   * @short MmData constructor, which gets fields values as parameters
   *
   * This constructor creates a MmData object and sets its fields to the values
   * passed to it
   */
        MmData( const QString& _mm_FolderName, const QString& _mm_FileName, const QString& _mm_Artist,
                const QString& _mm_Title, const QString& _mm_Album, const QString& _mm_Genre, const QString& _mm_Comment,
                const int _mm_Year, const int _mm_TrackNum,
                const unsigned long _mm_FileSize, const uint _mm_ModifiedTime, const bool _mm_isReadOnly,
                const bool _mm_isDir, const unsigned long _mm_SongLength, const uint _mm_SongChannles,
                const uint _mm_SampleRate, const uint _mm_BitRate )
            : MmFolder(_mm_FolderName), MmFileName(_mm_FileName), MmArtist(_mm_Artist), MmTitle(_mm_Title),
              MmAlbum(_mm_Album), MmGenre(_mm_Genre), MmComment(_mm_Comment), MmYear(_mm_Year), MmTrackNum(_mm_TrackNum),
              MmFileSize(_mm_FileSize), MmModifiedTime(_mm_ModifiedTime), MmIsReadOnly(_mm_isReadOnly), MmIsDir(_mm_isDir),
              MmLength(_mm_SongLength), MmChannels(_mm_SongChannles), MmSampleRate(_mm_SampleRate), MmBitRate(_mm_BitRate)
        {}

  /**
   * @short Some informative description
   *
   * Something that needs to be documented
   */
        QString Folder() const { return MmFolder; }

  /**
   * @short Some informative description
   *
   * Something that needs to be documented
   */
        QString FileName() const { return MmFileName; }

  /**
   * @short Some informative description
   *
   * Something that needs to be documented
   */
        QString Artist() const { return MmArtist; }

  /**
   * @short Some informative description
   *
   * Something that needs to be documented
   */
        QString Title() const { return MmTitle; }

  /**
   * @short Some informative description
   *
   * Something that needs to be documented
   */
        QString Album() const { return MmAlbum; }

  /**
   * @short Some informative description
   *
   * Something that needs to be documented
   */
        QString Genre() const { return MmGenre; }
/** Inline function that returns the stored Folder */
        QString Comment() const { return MmComment; }
/** Inline function that returns the stored Folder */
        int Year() const { return MmYear; }
/** Inline function that returns the stored Folder */
        int TrackNum() const { return MmTrackNum; }
/** Inline function that returns the stored Folder */
        unsigned long FileSize() const { return MmFileSize; }
/** Inline function that returns the stored Folder */
        uint ModifiedTime() const { return MmModifiedTime; }
/** Inline function that returns the stored Folder */
        bool IsReadOnly() const { return MmIsReadOnly; }
/** Inline function that returns the stored Folder */
        bool IsDir() const { return MmIsDir; }
/** Inline function that returns the stored Folder */
        unsigned long Length() const { return MmLength; }
/** Inline function that returns the stored Folder */
        uint Channels() const { return MmChannels; }
/** Inline function that returns the stored Folder */
        uint SampleRate() const { return MmSampleRate; }
/** Inline function that returns the stored Folder */
        uint BitRate() const { return MmBitRate; }

/** Inline function to set the stored folder name (internally QString)
 *  @param Fld The text to which to set this MmData's Folder
 *  @see MmData::MmFolder */
        void setFolder( const QString& Fld ) { MmFolder = Fld; }
/** Inline function to set the stored file name (internally QString)
 *  @param FN The text to which to set this MmData's FileName
 *  @see MmData::MmFileName */
        void setFileName( const QString& FN ) { MmFileName = FN; }
/** Inline function to set the stored artist (internally QString)
 *  @param AT The text to which to set this MmData's Artist
 *  @see MmData::MmArtist */
        void setArtist( const QString& AT ) { MmArtist = AT; }
/** Inline function to set the stored title (internally QString)
 *  @param TT The text name to which to set this MmData's Title
 *  @see MmData::MmTitle */
        void setTitle( const QString& TT ) { MmTitle = TT; }
/** Inline function to set the stored album name (internally QString)
 *  @param A The text to which to set this MmData's Album
 *  @see MmData::MmAlbum */
        void setAlbum( const QString& A ) { MmAlbum = A; }
/** Inline function to set the stored genre (internally QString)
 *  @param G The text to which to set this MmData's Genre
 *  @see MmData::MmGenre */
        void setGenre( const QString& G ) { MmGenre = G; }
/** Inline function to set the stored comment (internally QString)
 *  @param CMT The text to which to set this MmData's Comment field
 *  @see MmData::MmComment */
        void setComment( const QString& CMT ) { MmComment = CMT; }
/** Inline function to set the stored BitRate (internally uint) */
        void setYear( int y ) { MmYear = y; }
/** Inline function to set the stored BitRate (internally uint) */
        void setTrackNum( int n ) { MmTrackNum = n; }
/** Inline function to set the stored BitRate (internally uint) */
        void setFileSize( unsigned long i ) { MmFileSize = i; }
/** Inline function to set the stored BitRate (internally uint) */
        void setModifiedTime( uint t ) { MmModifiedTime = t; }
/** Inline function to set the stored BitRate (internally uint) */
        void setIsReadOnly( bool r ) { MmIsReadOnly = r; }
/** Inline function to set the stored BitRate (internally uint) */
        void setIsDir( bool d ) { MmIsDir = d; }
/** Inline function to set the stored BitRate (internally uint) */
        void setLength( unsigned long sl ) { MmLength = sl; }
/** Inline function to set the stored BitRate (internally uint) */
        void setChannels( uint sc ) { MmChannels = sc; }
/** Inline function to set the stored BitRate (internally uint) */
        void setSampleRate( uint sr ) { MmSampleRate = sr; }
/** Inline function to set the stored BitRate (internally uint) */
        void setBitRate( uint br ) { MmBitRate = br; }

    private:
/** Something that needs documentation */
        QString MmFolder;
/** Something that needs documentation */
        QString MmFileName;
/** Something that needs documentation */
        QString MmArtist;
/** Something that needs documentation */
        QString MmTitle;
/** Something that needs documentation */
        QString MmAlbum;
/** Something that needs documentation */
        QString MmGenre;
/** Something that needs documentation */
        QString MmComment;
/** Something that needs documentation */
        int MmYear;
/** Something that needs documentation */
        int MmTrackNum;

/** Something that needs documentation */
        unsigned long MmFileSize;
/** Something that needs documentation */
        uint MmModifiedTime;
/** Something that needs documentation */
        bool MmIsReadOnly;
/** Something that needs documentation */
        bool MmIsDir;

/** Something that needs documentation */
        unsigned long MmLength;
/** Something that needs documentation */
        uint MmChannels;
/** Something that needs documentation */
        uint MmSampleRate;
/** Something that needs documentation */
        uint MmBitRate;
    };

/**
 * MmDataList is actually a QValueList - uses explicit sharing.
 * This means that the list creates a copy of what we feed it;
 * For iteration over the list use these:
 * MmDataList::iterator
 * MmDataList::begin()
 * MmDataList::end()
 */
typedef QValueList<MmData> MmDataList;
/**
 * MmDataPList is a convinience list of pointers to MmData.
 * This means that the list has a pointer to each objects;
 * For iteration over the list use these:
 * MmDataPList::first()
 * MmDataPList::last()
 * MmDataPList::next()
 * MmDataPList::prev()
 * QPtrList can be set to not own the objects its pointers
 * point to by setAutoDelete( FALSE ); [DEFAULT]
 * @see QPtrList
 */
typedef QPtrList<MmData> MmDataPList;
/** Something that needs documentation */
typedef QPtrListIterator<MmData> MmDataPListIterator;

#endif // _KMK_MMDATA_H_
