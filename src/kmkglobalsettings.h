/***************************************************************************
 *   KMK - KDE Music Cataloger  -  the tool for personal                   *
 *                                 audio collection management             *
 *                                                                         *
 *   Copyright (C) 2006,2007 by Plamen Petrov                              *
 *   carpo@abv.bg                                                          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

/* This is a modified version of K3B's k3bglobalsettings.h to suit KMK's needs*/

#ifndef _KMK_GLOBAL_SETTINGS_H_
#define _KMK_GLOBAL_SETTINGS_H_

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/** debug constants - a byte of which bits define debug levels for
 *  KMK's subsystems
 *   KMK_DBG_NONE		0
 *   KMK_DBG_CONFIG		1
 *   KMK_DBG_CATALOG_IO		2
 *   KMK_DBG_CAT_MEM_OPS	4
 *   KMK_DBG_EXT_PLAYER		8
 *   KMK_DBG_FS_OPS		16
 *   KMK_DBG_SIGNALS		32
 *   KMK_DBG_SETTINGS		64       --need to see a more fit name/disambig. with *_CONFIG
 *   KMK_DBG_OTHER		128
 *   KMK_DBG_ALL		255
 */
#define   KMK_DBG_NONE		0
#define   KMK_DBG_CONFIG	1
#define   KMK_DBG_CATALOG_IO	2
#define   KMK_DBG_CAT_MEM_OPS	4
#define   KMK_DBG_EXT_PLAYER	8
#define   KMK_DBG_FS_OPS	16
#define   KMK_DBG_SIGNALS	32
#define   KMK_DBG_SETTINGS	64
#define   KMK_DBG_OTHER		128
#define   KMK_DBG_ALL		255

#include <qstring.h>

class KConfig;

/**
 * The class KmkGlobalSettings defines a set of variables, which hold
 * the current config used by KMK. Throughout the app these are accessed
 * like this :
 * \code
 *  if ( settings_obj->saveOnNavigate() ) { // do whats needed for saveOnNavigate }
 * \endcode
 * and here saveOnNavigate is defined as a private variable for KmkGlobalSettings.
 */
class KmkGlobalSettings
{
 public:
  KmkGlobalSettings();

  /**
   * This method takes care of reading the config group
   */
  void readSettings( KConfig* );

  /**
   * This method takes care of writinging the config group
   */
  void saveSettings( KConfig* );

  unsigned short dbg() const { return g_dbg; }
  bool saveOnNavigate() const { return g_save_on_navigate; }
  bool autoColumnWidth() const { return g_auto_column_width; }
  bool loadLast() const { return g_load_last; }
  bool saveGeo() const { return g_save_geo; }
  QString lastCatalogUsed() const { return g_last_catalog_used; }
  unsigned short extPlayer() const { return g_ext_player; }

  void setDbg( const unsigned short d ) { g_dbg = d; }
  void setSaveOnNavigate( const bool b ) { g_save_on_navigate = b; }
  void setAutoColumnWidth( const bool b ) { g_auto_column_width = b; }
  void setLoadLast( const bool b ) { g_load_last = b; }
  void setSaveGeo( const bool b ) { g_save_geo = b; }
  void setLastCatalogUsed( const QString s ) { g_last_catalog_used = s; }
  void setExtPlayer( const unsigned short d ) { g_ext_player = d; }

 private:
  unsigned short g_dbg;
  bool g_save_on_navigate;
  bool g_auto_column_width;
  bool g_load_last;
  bool g_save_geo;
  QString g_last_catalog_used;
  unsigned short g_ext_player;
};

#endif //_KMK_GLOBAL_SETTINGS_H_
